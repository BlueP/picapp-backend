﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace AppPic.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class DefaultController : ControllerBase
    {
        [Route("/")]
        [Route("/swagger")]
        public RedirectResult Index()
        {
            return new RedirectResult("~/swagger");
        }
    }

    public class SystemNotification
    {
        public static HttpResponseMessage SendExceptionToLineNotify(string customMessage, Exception objError, string token)
        {
            StringBuilder sb = new StringBuilder();
            if (objError != null)
            {
                if (!string.IsNullOrWhiteSpace(customMessage))
                {
                    sb.Append(string.Format("Message:{0}\r\n", customMessage));
                }

                sb.Append("Exception Data:\r\n");
                sb.Append("====================\r\n");
                sb.Append(String.Format("Message: {0}\r\n", objError.Message));
                sb.Append(String.Format("StackTrace: {0}\r\n\r\n", objError.StackTrace));

                if (objError.InnerException != null)
                {
                    sb.Append("Inner Exception Data:\r\n");
                    sb.Append("====================\r\n");
                    sb.Append(String.Format("Message: {0}\r\n", objError.InnerException.Message));
                    sb.Append(String.Format("StackTrace: {0}\r\n\r\n", objError.InnerException.StackTrace));
                }
            }
            return SendLineNotify(sb.ToString(), token);
        }

        public static HttpResponseMessage SendLineNotify(string msg, string customToken = "")
        {
            try
            {
                var token = string.IsNullOrEmpty(customToken) ? "pi1yVHWoC1zIfc827C452MKQxmPH2sBpiG5ZG7f78lU" : customToken;
                var client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                var content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("message", msg)
                });
                return client.PostAsync("https://notify-api.line.me/api/notify", content).Result;
            }
            catch (Exception)
            {
                return new HttpResponseMessage(System.Net.HttpStatusCode.ServiceUnavailable);
            }
        }
    }
}
