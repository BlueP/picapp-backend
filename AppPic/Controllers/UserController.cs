﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using AppPic.Controllers;
using AppPic.DTOs;
using Firebase.Auth;
using Firebase.Storage;
using FirebaseAuthentication.Providers;
using Google.Cloud.Storage.V1;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Pic.DTOs;
using Pic.Services;
using RestSharp;
using RestSharp.Authenticators;
using FirebaseAuthProvider = Firebase.Auth.FirebaseAuthProvider;
using StatusCodes = AppPic.DTOs.StatusCodes;

namespace Pic.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserData _repository;
        private readonly IConfiguration _configuration;
        private static string ApiKey = "AIzaSyAn_g2_su6kogGPMcthaHhSdcM5UyPxGbo";
        private static string Bucket = "picture-13e1b.appspot.com";
        private static string AuthEmail = "ty.worksiri@gmail.com";
        private static string AuthPassword = "test1150";

        public UserController(IUserData repository, IConfiguration configuration)
        {
            this._repository = repository;
            this._configuration = configuration;
        }

        #region list of mime types
        private static IDictionary<string, string> _mappings = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase) {
        {".art", "image/x-jg"},
        {".bmp", "image/bmp"},
        {".cmx", "image/x-cmx"},
        {".cod", "image/cis-cod"},
        {".dib", "image/bmp"},
        {".gif", "image/gif"},
        {".ico", "image/x-icon"},
        {".ief", "image/ief"},
        {".jfif", "image/pjpeg"},
        {".jpe", "image/jpeg"},
        {".jpeg", "image/jpeg"},
        {".jpg", "image/jpeg"},
        {".mac", "image/x-macpaint"},
        {".pbm", "image/x-portable-bitmap"},
        {".pct", "image/pict"},
        {".pgm", "image/x-portable-graymap"},
        {".pic", "image/pict"},
        {".pict", "image/pict"},
        {".png", "image/png"},
        {".pnm", "image/x-portable-anymap"},
        {".pnt", "image/x-macpaint"},
        {".pntg", "image/x-macpaint"},
        {".pnz", "image/png"},
        {".ppm", "image/x-portable-pixmap"},
        {".qti", "image/x-quicktime"},
        {".qtif", "image/x-quicktime"},
        {".ras", "image/x-cmu-raster"},
        {".rf", "image/vnd.rn-realflash"},
        {".rgb", "image/x-rgb"},
        {".tif", "image/tiff"},
        {".tiff", "image/tiff"},
        {".wbmp", "image/vnd.wap.wbmp"},
        {".wdp", "image/vnd.ms-photo"},
        {".xbm", "image/x-xbitmap"},
        {".xpm", "image/x-xpixmap"},
        {".xwd", "image/x-xwindowdump"},
        };

        public static string GetMimeType(string extension)
        {
            if (extension == null)
            {
                throw new ArgumentNullException("extension");
            }

            string mime;

            return _mappings.TryGetValue(extension, out mime) ? mime : ".jpeg";
        }
        #endregion

        #region Token generator
        private UserToken BuildToken(UserInfo data, string guid)
        {
            var claims = new List<Claim>()
            {
                new Claim(ClaimTypes.Sid, guid),
                new Claim(ClaimTypes.Email, data.Email ?? " "),
                new Claim(ClaimTypes.MobilePhone, data.PhoneNumber ?? " "),
                new Claim(ClaimTypes.NameIdentifier, data.User_ID.ToString()),
                new Claim("mykey", _configuration["JWT:mykeyUser"])
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var expiration = DateTime.UtcNow.AddYears(1);

            JwtSecurityToken token = new JwtSecurityToken(
                issuer: null,
                audience: null,
                claims: claims,
                expires: expiration,
                signingCredentials: creds);

            return new UserToken()
            {
                Token = new JwtSecurityTokenHandler().WriteToken(token),
                Expiration = expiration
            };
        }


        #endregion

        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {

                return false;
            }
        }

        private IRestResponse SendOTPMessage(string otp, string referKey, string email)
        {
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://api.mailgun.net/v3");
            client.Authenticator =
                new HttpBasicAuthenticator("api", _configuration["mailgun:key"]);
            RestRequest request = new RestRequest();
            request.AddParameter("domain", _configuration["mailgun:domain"], ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", _configuration["mailgun:mailname"]);
            request.AddParameter("subject", "Verify Code");
            request.AddParameter("to", email);
            var body = new StringBuilder();
            body.AppendLine("your verify code is " + otp);
            body.AppendLine(" ");
            body.AppendLine("it expires in 30 minutes. Do not share it with anyone.");
            body.AppendLine(referKey);
            request.AddParameter("text", body.ToString());
            request.Method = Method.POST;
            return client.Execute(request);
        }
        
        private IRestResponse SendOTPMessageResetPw(string otp, string referKey, UserInfo user)
        {
            RestClient client = new RestClient();
            EmailForgotPassword _email = new EmailForgotPassword();
            client.BaseUrl = new Uri("https://api.mailgun.net/v3");
            client.Authenticator =
                new HttpBasicAuthenticator("api", _configuration["mailgun:key"]);
            RestRequest request = new RestRequest();
            request.AddParameter("domain", _configuration["mailgun:domain"], ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", _configuration["mailgun:mailname"]);

            request.AddParameter("to", user.Email);
            request.AddParameter("template", "forgot-password");
            _email.button = new EmailForgotPasswordBotton();
            _email.customerInfo = new EmailForgotPasswordCustomerInfo();
            _email.from = new EmailFrom();
            _email.footer = new EmailFooter();

            request.AddParameter("subject", "Forgot Password");
            _email.topic = "Dear " + user.Name;
            _email.title1 = "You are receiving this email because you requested a password reset for your AppPic account.";
            _email.title2 = "If this is not the case then please disregard this email, otherwise please use this verification code to reset your password.";
            _email.button.otp = otp;
            _email.customerInfo.title = "Your Username is: ";
            _email.customerInfo.name = user.Email;
            _email.title3 = "Thanks for supporting my AppPic!";
            _email.from.title = "Best,";

            //if (customer.AppLanguage == AppLanguageSelect.Englist)
            //{
            //    request.AddParameter("subject", "Forgot Password");
            //    _email.topic = "Dear " + customer.Firstname;
            //    _email.title1 = "You are receiving this email because you requested a password reset for your LaCarte account.";
            //    _email.title2 = "If this is not the case then please disregard this email, otherwise please use this verification code to reset your password.";
            //    _email.button.otp = otp;
            //    _email.customerInfo.title = "Your Username is: ";
            //    _email.customerInfo.name = customer.Email;
            //    _email.title3 = "Thanks for supporting your local restaurants!";
            //    _email.from.title = "Best,";
            //}
            //else
            //{
            //    request.AddParameter("subject", "LaCarte - パスワードのリセット");
            //    _email.topic = customer.Firstname;
            //    _email.title1 = "LaCarteアカウントについて、パスワードのリセットのリクエストがありました。下記のリンクをクリックし、操作を続けてください。";
            //    _email.title2 = "リセットのリクエストをされた覚えがない場合はこのメールを無視してください。";
            //    _email.button.otp = otp;
            //    _email.customerInfo.title = "ユーザーネーム: ";
            //    _email.customerInfo.name = customer.Email;
            //    _email.title3 = "LaCarteをご利用頂きどうもありがとうございます。";
            //    _email.from.title = "今後ともよろしくお願いいたします！";
            //}

            _email.from.name = "The AppPic team";
            _email.footer.address = "";
            _email.footer.webUrl = "https://lacartemenu.com";
            _email.footer.facebookUrl = "https://www.facebook.com/LaCarteJP";
            _email.footer.instagramUrl = "https://www.instagram.com/lacartejp/";
            string _template = "{\"topic\": \"" + _email.topic + "\", \"title1\": \"" + _email.title1 + "\", \"title2\": \"" + _email.title2 + "\", " +
            " \"button\": {\"otp\": \"" + _email.button.otp + "\"}, " +
            " \"customerInfo\": {\"title\": \"" + _email.customerInfo.title + "\",\"name\": \"" + _email.customerInfo.name + "\"}, " +
            " \"title3\": \"" + _email.title3 + "\", " +
            " \"from\": {\"title\": \"" + _email.from.title + "\",\"name\": \"" + _email.from.name + "\"}, " +
            " \"footer\": {\"address\": \"" + _email.footer.address + "\",\"webUrl\": \"" + _email.footer.webUrl + "\",\"facebookUrl\": \"" + _email.footer.facebookUrl + "\",\"instagramUrl\": \"" + _email.footer.instagramUrl + "\"}} ";
            request.AddParameter("h:X-Mailgun-Variables", _template);
            request.Method = Method.POST;
            return client.Execute(request);
        }

        private IRestResponse SendOTPMessageSignUp(string otp, string referKey, UserInfo user)
        {
            RestClient client = new RestClient();
            EmailForgotPassword _email = new EmailForgotPassword();
            client.BaseUrl = new Uri("https://api.mailgun.net/v3");
            client.Authenticator =
                new HttpBasicAuthenticator("api", _configuration["mailgun:key"]);
            RestRequest request = new RestRequest();
            request.AddParameter("domain", _configuration["mailgun:domain"], ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", _configuration["mailgun:mailname"]);

            request.AddParameter("to", user.Email);
            request.AddParameter("template", "signup");
            _email.button = new EmailForgotPasswordBotton();
            _email.customerInfo = new EmailForgotPasswordCustomerInfo();
            _email.from = new EmailFrom();
            _email.footer = new EmailFooter();

            request.AddParameter("subject", "Signup");
            _email.topic = "Dear " + user.Name;
            _email.title1 = "You are receiving this email because you signup for your AppPic account.";
            _email.title2 = "If this is not the case then please disregard this email, otherwise please use this verification code to signup.";
            _email.button.otp = otp;
            _email.customerInfo.title = "Your Username is: ";
            _email.customerInfo.name = user.Email;
            _email.title3 = "Thanks for supporting my AppPic!";
            _email.from.title = "Best,";

            //if (customer.AppLanguage == AppLanguageSelect.Englist)
            //{
            //    request.AddParameter("subject", "Forgot Password");
            //    _email.topic = "Dear " + customer.Firstname;
            //    _email.title1 = "You are receiving this email because you requested a password reset for your LaCarte account.";
            //    _email.title2 = "If this is not the case then please disregard this email, otherwise please use this verification code to reset your password.";
            //    _email.button.otp = otp;
            //    _email.customerInfo.title = "Your Username is: ";
            //    _email.customerInfo.name = customer.Email;
            //    _email.title3 = "Thanks for supporting your local restaurants!";
            //    _email.from.title = "Best,";
            //}
            //else
            //{
            //    request.AddParameter("subject", "LaCarte - パスワードのリセット");
            //    _email.topic = customer.Firstname;
            //    _email.title1 = "LaCarteアカウントについて、パスワードのリセットのリクエストがありました。下記のリンクをクリックし、操作を続けてください。";
            //    _email.title2 = "リセットのリクエストをされた覚えがない場合はこのメールを無視してください。";
            //    _email.button.otp = otp;
            //    _email.customerInfo.title = "ユーザーネーム: ";
            //    _email.customerInfo.name = customer.Email;
            //    _email.title3 = "LaCarteをご利用頂きどうもありがとうございます。";
            //    _email.from.title = "今後ともよろしくお願いいたします！";
            //}

            _email.from.name = "The AppPic team";
            _email.footer.address = "";
            _email.footer.webUrl = "https://lacartemenu.com";
            _email.footer.facebookUrl = "https://www.facebook.com/LaCarteJP";
            _email.footer.instagramUrl = "https://www.instagram.com/lacartejp/";
            string _template = "{\"topic\": \"" + _email.topic + "\", \"title1\": \"" + _email.title1 + "\", \"title2\": \"" + _email.title2 + "\", " +
            " \"button\": {\"otp\": \"" + _email.button.otp + "\"}, " +
            " \"customerInfo\": {\"title\": \"" + _email.customerInfo.title + "\",\"name\": \"" + _email.customerInfo.name + "\"}, " +
            " \"title3\": \"" + _email.title3 + "\", " +
            " \"from\": {\"title\": \"" + _email.from.title + "\",\"name\": \"" + _email.from.name + "\"}, " +
            " \"footer\": {\"address\": \"" + _email.footer.address + "\",\"webUrl\": \"" + _email.footer.webUrl + "\",\"facebookUrl\": \"" + _email.footer.facebookUrl + "\",\"instagramUrl\": \"" + _email.footer.instagramUrl + "\"}} ";
            request.AddParameter("h:X-Mailgun-Variables", _template);
            request.Method = Method.POST;
            return client.Execute(request);
        }

        private IRestResponse SendOTPWelcomeMessage(UserInfo user)
        {
            RestClient client = new RestClient();

            client.BaseUrl = new Uri("https://api.mailgun.net/v3");
            client.Authenticator =
                new HttpBasicAuthenticator("api", _configuration["mailgun:key"]);
            RestRequest request = new RestRequest();
            request.AddParameter("domain", _configuration["mailgun:domain"], ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", _configuration["mailgun:mailname"]);
            request.AddParameter("to", user.Email);
            request.AddParameter("template", "welcome-email-customer");

            EmailWelcomeCustomer _email = new EmailWelcomeCustomer();
            _email.button = new EmailWelcomeCustomerBotton();
            _email.from = new EmailFrom();
            _email.footer = new EmailFooter();

            request.AddParameter("subject", "Welcome Email Customer");
            _email.topic = "Dear " + user.Name;
            _email.title1 = "Thank you!!";
            _email.title2 = "for confirming your email address.";
            _email.title3 = "Click the map below";
            _email.title4 = "";
            _email.title5 = "We hope you enjoy AppPic!";
            _email.button.title = "Explore";
            _email.button.url = "https://lacarte.onelink.me/x6nK/9a6818d9";
            _email.from.title = "Best,";

            //if (customer.AppLanguage == AppLanguageSelect.Englist)
            //{
            //    request.AddParameter("subject", "Welcome Email Customer");
            //    _email.topic = "Dear " + customer.Firstname;
            //    _email.title1 = "Thank you!!";
            //    _email.title2 = "for confirming your email address. You’re all set and ready to start ordering.";
            //    _email.title3 = "Click the map below";
            //    _email.title4 = "to find the restaurants nearest to you!";
            //    _email.title5 = "We hope you enjoy your meal!";
            //    _email.button.title = "Explore";
            //    _email.button.url = "https://lacarte.onelink.me/x6nK/9a6818d9";
            //    _email.from.title = "Best,";
            //}
            //else
            //{
            //    request.AddParameter("subject", "LaCarte - 会員登録が完了しました");
            //    _email.topic = customer.Firstname;
            //    _email.title1 = "会員登録が完了いたしました！";
            //    _email.title2 = "メールアドレスをご確認いただきどうもありがとうございました。LaCarteのご利用の準備ができました。";
            //    _email.title3 = "マップをクリック！";
            //    _email.title4 = "近くのお店を探そう";
            //    _email.title5 = "";
            //    _email.button.title = "お店を探す";
            //    _email.button.url = " https://lacarte.onelink.me/x6nK/9a6818d9";
            //    _email.from.title = "今後ともよろしくお願いいたします！";
            //}

            _email.from.name = "The AppPic team";
            _email.footer.address = "";
            _email.footer.webUrl = "https://lacartemenu.com";
            _email.footer.facebookUrl = "https://www.facebook.com/LaCarteJP";
            _email.footer.instagramUrl = "https://www.instagram.com/lacartejp/";
            string _template = "{\"topic\": \"" + _email.topic + "\", \"title1\": \"" + _email.title1 + "\", \"title2\": \"" + _email.title2 + "\", " +
            " \"title3\": \"" + _email.title3 + "\", \"title4\": \"" + _email.title4 + "\", \"title5\": \"" + _email.title5 + "\"," +
            " \"button\": {\"title\": \"" + _email.button.title + "\",\"url\": \"" + _email.button.url + "\"}, " +
            " \"from\": {\"title\": \"" + _email.from.title + "\",\"name\": \"" + _email.from.name + "\"}, " +
            " \"footer\": {\"address\": \"" + _email.footer.address + "\",\"webUrl\": \"" + _email.footer.webUrl + "\",\"facebookUrl\": \"" + _email.footer.facebookUrl + "\",\"instagramUrl\": \"" + _email.footer.instagramUrl + "\"}} ";
            request.AddParameter("h:X-Mailgun-Variables", _template);
            request.Method = Method.POST;
            return client.Execute(request);
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<ActionResult<Result>> PostLogin(UserLogin data)
        {
            Result resData = new Result();
            UserInfo _data = new UserInfo();
            string guid = Guid.NewGuid().ToString("n");
            bool logStatus = false;
            try
            {
                _data = await _repository.GetLogin(data);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (_data == null)
                return Unauthorized("username or password incorrect");

            try
            {
                logStatus = await _repository.PostUserLog(_data, guid);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            if (logStatus == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
            }
            else
            {
                //await SendNotiWarning(_data.CUS_ID);
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = BuildToken(_data, guid);
            }
            return resData;
        }
        [AllowAnonymous]
        [HttpPost("usernamecheck")]
        public async Task<ActionResult<Result>> PostUsernameCheck(UsernameCheck data)
        {
            Result resData = new Result();
            int status = 0;
            try
            {
                status = await _repository.PostUsernameCheck(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            if (status == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Username is duplicate";
            }
            else if (status == 1)
            {
                // Send username //
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Can user this username";

            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Can not check username";

            }
            return resData;
        }
        [AllowAnonymous]
        [HttpPost("emailcheck")]
        public async Task<ActionResult<Result>> PostEmailCheck(UserEmail data)
        {
            Result resData = new Result();
            int status = 0;
            try
            {
                status = await _repository.PostEmailCheck(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            if (status == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Email or Phone number is duplicate";
            }
            else if (status == 1)
            {
                // Send email //
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Can user this email";

            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Can not check email";

            }
            return resData;
        }

        [AllowAnonymous]
        [HttpPost("signup")]
        public async Task<ActionResult<Result>> PostRegister(UserSignup data)
        {
            Result resData = new Result();
            int status = 0;
            try
            {
                status = await _repository.PostUserRegister(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            if (status == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Email or Phone number is duplicate";
            }
            else if (status == 1)
            {
                //var userSignup = new UserSignup();
                //userSignup.Email = data.Email;
                //userSignup.Username = data.Username;
                //userSignup.Password = data.Password;
                var userLogin = new UserLogin();
                userLogin.Email = data.Email;
                userLogin.Password = data.Password;

                UserInfo _data = new UserInfo();
                bool logStatus = false;
                string guid = Guid.NewGuid().ToString("n");
                try
                {
                    _data = await _repository.GetLogin(userLogin);
                    logStatus = await _repository.PostUserLog(_data, guid);
                    /*if (_data.Email != null)
                        SendOTPWelcomeMessage(_data);*/
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
                if (logStatus == false)
                {
                    resData.StatusCode = (int)(StatusCodes.Error);
                    resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                }
                else
                {
                    //await SendNotiWarning(_data.CUS_ID);
                    resData.StatusCode = (int)(StatusCodes.Succuss);
                    resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                    resData.Results = BuildToken(_data, guid);
                }
            }
            else if (status == 2)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Can not register";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Can not register";

            }
            return resData;
        }

        [AllowAnonymous]
        [HttpPost("SendOTPPasswordToEmail")]
        public async Task<ActionResult<Result>> GetSendOTPPasswordToEmail(UserResetPasswordToEmail data)
        {
            Result resData = new Result();
            UserInfo _data;
            UserPasswordReset reqData = new UserPasswordReset();
            Random generator = new Random();
            RandomGenerator generatorString = new RandomGenerator();
            DateTime expDate = DateTime.UtcNow.AddHours(9).AddMinutes(30);
            bool status = false;
            string OTP = generator.Next(0, 999999).ToString("D6");
            string referenceKey = generatorString.RandomString(6);

            try
            {
                _data = await _repository.GetCustomerByPhoneOrEmail(data.Email);
            }
            catch (Exception ex)
            {
                //var lineNoti = new StringBuilder();
                //lineNoti.Append(" Customer GetSendOTPPasswordToEmail");
                //lineNoti.AppendLine(" ");
                //lineNoti.AppendLine(ex.Message);
                //SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }
            if (_data == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "email or phonenumber invalid";
                return resData;
            }

            reqData.User_ID     = _data.User_ID;
            reqData.Token_Key   = referenceKey;
            reqData.Verify_Code = OTP;
            reqData.ExpireDate  = expDate;

            try
            {
                status = await _repository.GetSendOTPPasswordToEmail(reqData);
            }
            catch (Exception ex)
            {
                //var lineNoti = new StringBuilder();
                //lineNoti.Append(" Customer UserSendOTPResetPasswordToEmail");
                //lineNoti.AppendLine(" ");
                //lineNoti.AppendLine(ex.Message);
                //SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {
                var _user = await _repository.GetProfile(_data.User_ID);
                if (IsValidEmail(data.Email) == true)
                {
                    SendOTPMessageResetPw(OTP, referenceKey, _user);
                }
                //else
                //{
                //    SendPhoneOTPResetPw(OTP, referenceKey, _data.PhoneNumber);
                //}
                ResultOtp resOtp = new ResultOtp();
                resOtp.Reference_Key = referenceKey;
                resOtp.ExpireDate = expDate;
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "";
                resData.Results = resOtp;
            }
            return resData;
        }

        [AllowAnonymous]
        [HttpPost("CheckOTPPasswordFromwWithEmail")]
        public async Task<ActionResult<Result>> CheckOTPPasswordFromwWithEmail(CheckOtpResetPwWithEmail data)
        {
            Result resData = new Result();
            int status = 0;
            try
            {
                status = await _repository.PostCheckOTPPasswordFromWithEmail(data);
            }
            catch (Exception ex)
            {
                //var lineNoti = new StringBuilder();
                //lineNoti.Append(" Customer PostCheckOTPPasswordFromEmail");
                //lineNoti.AppendLine(" ");
                //lineNoti.AppendLine(ex.Message);
                //SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == 2)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "change password success";

            }
            else if (status == 1)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "OTP expired";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "OTP invalid";
            }
            return resData;
        }

        [AllowAnonymous]
        [HttpPost("CheckOTPPasswordFromwWithOutEmail")]
        public async Task<ActionResult<Result>> CheckOTPPasswordFromwWithOutEmail(CheckOtpResetPwWithOutEmail data)
        {
            Result resData = new Result();
            int status = 0;
            try
            {
                status = await _repository.PostCheckOTPPasswordFromWithOutEmail(data);
            }
            catch (Exception ex)
            {
                //var lineNoti = new StringBuilder();
                //lineNoti.Append(" Customer PostCheckOTPPasswordFromEmail");
                //lineNoti.AppendLine(" ");
                //lineNoti.AppendLine(ex.Message);
                //SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            //if (status == 2)
            //{
            //    resData.StatusCode = (int)(StatusCodes.Succuss);
            //    resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
            //    resData.Messages = "confirm otp code success";

            //}
            if (status == 1)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "confirm otp code success";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "OTP invalid";
            }
            return resData;
        }

        [AllowAnonymous]
        [HttpPost("SendOTPSigUpToEmail")]
        public async Task<ActionResult<Result>> GetSendOTPSignUpToEmail(UserSignUp data)
        {
            Result resData = new Result();
            UserInfo _data;
            UserSignUp reqData = new UserSignUp();
            Random generator = new Random();
            RandomGenerator generatorString = new RandomGenerator();
            DateTime expDate = DateTime.UtcNow.AddHours(9).AddMinutes(30);
            bool status = false;
            string OTP = generator.Next(0, 999999).ToString("D6");
            string referenceKey = generatorString.RandomString(6);

            try
            {
                _data = await _repository.GetCustomerByPhoneOrEmail(data.Email);
            }
            catch (Exception ex)
            {
                //var lineNoti = new StringBuilder();
                //lineNoti.Append(" Customer GetSendOTPPasswordToEmail");
                //lineNoti.AppendLine(" ");
                //lineNoti.AppendLine(ex.Message);
                //SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }
            if (_data == null)
            {
                //reqData.User_ID = _data.User_ID;
                reqData.Token_Key = referenceKey;
                reqData.Verify_Code = OTP;
                reqData.ExpireDate = expDate;
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "email or phonenumber duplicate";
                return resData;
            }


            try
            {
                status = await _repository.GetSendOTPSignUpToEmail(reqData);
            }
            catch (Exception ex)
            {
                //var lineNoti = new StringBuilder();
                //lineNoti.Append(" Customer UserSendOTPResetPasswordToEmail");
                //lineNoti.AppendLine(" ");
                //lineNoti.AppendLine(ex.Message);
                //SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data invalid";
            }
            else
            {
                var _user = await _repository.GetProfile(_data.User_ID);
                if (IsValidEmail(data.Email) == true)
                {
                    SendOTPMessageSignUp(OTP, referenceKey, _user);
                }
                //else
                //{
                //    SendPhoneOTPResetPw(OTP, referenceKey, _data.PhoneNumber);
                //}
                ResultOtp resOtp = new ResultOtp();
                resOtp.Reference_Key = referenceKey;
                resOtp.ExpireDate = expDate;
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "";
                resData.Results = resOtp;
            }
            return resData;
        }

        [AllowAnonymous]
        [HttpPost("CheckOTPSignUpFromwWithEmail")]
        public async Task<ActionResult<Result>> CheckOTPSignUpFromWithEmail(CheckOtpSignUpWithEmail data)
        {
            Result resData = new Result();
            int status = 0;
            try
            {
                status = await _repository.PostCheckOTPSignUpFromWithEmail(data);
            }
            catch (Exception ex)
            {
                //var lineNoti = new StringBuilder();
                //lineNoti.Append(" Customer PostCheckOTPPasswordFromEmail");
                //lineNoti.AppendLine(" ");
                //lineNoti.AppendLine(ex.Message);
                //SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            if (status == 2)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "signup success";

            }
            else if (status == 1)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "email duplicate";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "OTP invalid";
            }
            return resData;
        }

        [AllowAnonymous]
        [HttpPost("CheckOTPSignUpFromwWithOutEmail")]
        public async Task<ActionResult<Result>> CheckOTPSignUpFromwWithOutEmail(CheckOtpSignUpWithOutEmail data)
        {
            Result resData = new Result();
            int status = 0;
            try
            {
                status = await _repository.PostCheckOTPSignUpFromWithOutEmail(data);
            }
            catch (Exception ex)
            {
                //var lineNoti = new StringBuilder();
                //lineNoti.Append(" Customer PostCheckOTPPasswordFromEmail");
                //lineNoti.AppendLine(" ");
                //lineNoti.AppendLine(ex.Message);
                //SystemNotification.SendLineNotify(lineNoti.ToString());
                return BadRequest(ex.Message);
            }

            //if (status == 2)
            //{
            //    resData.StatusCode = (int)(StatusCodes.Succuss);
            //    resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
            //    resData.Messages = "confirm otp code success";

            //}
            if (status == 1)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "confirm otp code success";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "OTP invalid";
            }
            return resData;
        }

        [HttpPost("changepassword")]
        public async Task<ActionResult<Result>> PostChangePassword(UserPassword data)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int user_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);         
            try
            {
                status = await _repository.PostPasswordChange(data, user_id);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            if (user_id == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Results = "token false";
            }
            else if(status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "current password is isvalid";
            }
            else if (status == true)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = "change password success";
            }
            else
            {
                    //await SendNotiWarning(_data.CUS_ID);
                    resData.StatusCode = (int)(StatusCodes.Succuss);
                    resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                    resData.Results = "data is null";
            }
            return resData;
        }

        [HttpGet("profile")]
        public async Task<ActionResult<Result>> GetProfile()
        {
            Result resData = new Result();
            UserInfo _data = new UserInfo();
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int user_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                _data = await _repository.GetProfile(user_id);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (user_id == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Results = "token false";
            }
            else if (_data == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
            }
            else
            {
                //await SendNotiWarning(_data.CUS_ID);
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = _data;
            }
 
            return resData;
        }

        [HttpPut("profile")]
        public async Task<ActionResult<Result>> PutProfile(UserUpdate data)
        {
            Result resData = new Result();
            int status = 0;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int user_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                status = await _repository.PutProfile(data, user_id);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (user_id == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Results = "token false";
            }
            else if (status == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Email or Phone number or Username is duplicate";
            }
            else if (status == 1)
            {
                //await SendNotiWarning(_data.CUS_ID);
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = "update profile success";
            }
            else if (status == 2)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Can not update";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Can not update";
            }

            return resData;
        }

        [HttpGet("allusername")]
        public async Task<ActionResult<Result>> GetUsername(string name)
        {
            Result resData = new Result();
            List<UserName> _data = new List<UserName>();
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int user_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                _data = await _repository.GetAllUsername(user_id,name);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (user_id == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Results = "token false";
            }
            else if (_data == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
            }
            else
            {
                //await SendNotiWarning(_data.CUS_ID);
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = _data;
            }

            return resData;
        }
        [AllowAnonymous]
        [HttpGet("getUserByID")]
        public async Task<ActionResult<Result>> GetUserByID(int user_id)
        {
            Result resData = new Result();
            UserName _data;
            try
            {
                _data = await _repository.GetUserByID(user_id);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (_data == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Data is Null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = _data;
            }
            return resData;
        }

        /* [HttpPost("uploadImageProfile"),DisableRequestSizeLimit]
         public async Task<ActionResult<Result>> PostUploadProfileImage([FromForm] UploadProfileImage _data)
         {
             string errMsg = "";
             bool status = false;
             Result resData = new Result();
             ResultUploadImage result = new ResultUploadImage();
             var identity = HttpContext.User.Identity as ClaimsIdentity;
             int user_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
             try
             {
                 var file = _data.Image;
                 var folderName = Path.Combine("Images", "ProfileImage");
                 var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                 var data = await _repository.GetUserByID(user_id);
                 if (data == null)
                     return BadRequest();
                 var contentType = GetMimeType(file.ContentType);

                 if (file.Length > 0)
                 {
                     string itemname = "User_ID-" + user_id  + contentType;
                     var FullPath = Path.Combine(pathToSave, itemname);
                     if (System.IO.File.Exists(FullPath))
                     {
                         System.IO.File.Delete(FullPath);
                     }
                     var dbPath = Path.Combine(folderName, itemname);
                     using (var stream = new FileStream(FullPath, FileMode.Create))
                     {
                         file.CopyTo(stream);
                     }
                     result.FileName = itemname;
                     result.ImagePath = _configuration["imagePath:profilesImage"] + itemname;
                 }
                 else
                 {
                     return BadRequest();
                 }
             }
             catch (Exception ex)
             {

                 return BadRequest(ex.Message);
             }
             try
             {
                 status = await _repository.PutUserUploadImage(result, user_id);
             }
             catch (Exception ex)
             {

                 return BadRequest(ex.Message);
             }
             if (errMsg != "" || status == false)
             {
                 resData.StatusCode = (int)(StatusCodes.Error);
                 resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                 resData.Messages = errMsg;
             }
             else
             {
                 resData.StatusCode = (int)(StatusCodes.Succuss);
                 resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                 resData.Results = result;
             }
             return resData;
         }*/
        [HttpPost("uploadImageProfile"), DisableRequestSizeLimit]
        public async Task<ActionResult<Result>> PostUploadImageProfile([FromForm] UploadProfileImage _data)
        {
            string errMsg = "";
            bool status = false;
            Result resData = new Result();
            ResultUploadImage result = new ResultUploadImage();
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int user_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                var folderName = Path.Combine("Images", "ProfileImage");
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                var fileupload = _data.Image;
                var contentType = GetMimeType(fileupload.ContentType);
                FileStream fileStream = null;

                if (fileupload.Length > 0)
                {
                    string itemname = "User_ID-" + user_id + contentType;
                    var FullPath = Path.Combine(pathToSave, itemname);
                    if (System.IO.File.Exists(FullPath))
                    {
                        System.IO.File.Delete(FullPath);
                    }
                    //Upload the file to firebase
                    using (fileStream = new FileStream(FullPath, FileMode.Create))
                    {
                        fileupload.CopyTo(fileStream);
                    }                    
                   fileStream = new FileStream(FullPath, FileMode.Open);
                    // Firebase uploading stuffs
                    var auth = new FirebaseAuthProvider(new FirebaseConfig(ApiKey));
                    var a = await auth.SignInWithEmailAndPasswordAsync(AuthEmail,AuthPassword);
                    //Cancellation token
                    var cancellation = new CancellationTokenSource();
                    var upload = new FirebaseStorage(Bucket,
                        new FirebaseStorageOptions
                        {
                            AuthTokenAsyncFactory = () => Task.FromResult(a.FirebaseToken),
                            ThrowOnCancel = true
                        })
                        .Child("Profile")
                        .Child(""+user_id+"")
                        .Child($"{itemname}")
                        .PutAsync(fileStream,cancellation.Token);
                    string link = await upload;
                    await fileStream.DisposeAsync();
                    System.IO.File.Delete(FullPath);// Delete Image File in Folder Image
                    result.FileName = itemname;
                    result.ImagePath = link;
                }
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            try
            {
                status = await _repository.PutUserUploadImage(result, user_id);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (errMsg != "" || status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = errMsg;
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = result;
            }
            return resData;
        }

        [HttpGet("getMyPost")]
        public async Task<ActionResult<Result>> GetMyPost()
        {
            Result resData = new Result();
            List<ResultCreatePost> _data;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int user_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                _data = await _repository.GetMyPost(user_id);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (user_id == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Results = "token false";
            }
            else if (_data == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
            }
            else
            {
                //await SendNotiWarning(_data.CUS_ID);
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = _data;
            }

            return resData;
        }

        [HttpPost("createPost"), DisableRequestSizeLimit]
        public async Task<ActionResult<Result>> PostCreatePost([FromForm] UserPost _data, [FromForm] IFormFileCollection uploads)
        {
            bool statusPost = false;
            bool statusImage = false;
            //bool status = false;
            PostID PostID;
            Result resData = new Result();
            ResultCreatePost result = new ResultCreatePost();
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int user_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                DateTime createdate = DateTime.Now;
                statusPost = await _repository.CreatePost(_data, user_id,createdate);
                PostID = await _repository.CheckPostHaveInDatabase(user_id, createdate);
                var folderName = Path.Combine("Images", "ProfileImage");
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                FileStream fileStream = null;
                int i = 0;
                foreach (var item in uploads)
                {
                    var fileupload = item;
                    var contentType = GetMimeType(fileupload.ContentType);

                    if (fileupload.Length > 0)
                    {

                        string itemname = "User_ID-" + user_id + "-" + PostID.Post_ID + "-" + DateTime.Now.ToString("yyyyMMddHHmmss") + contentType;
                        var FullPath = Path.Combine(pathToSave, itemname);
                        //Upload the file to firebase
                        using (fileStream = new FileStream(FullPath, FileMode.Create))
                        {
                            fileupload.CopyTo(fileStream);
                        }
                        fileStream = new FileStream(FullPath, FileMode.Open);
                        // Firebase uploading stuffs
                        var auth = new FirebaseAuthProvider(new FirebaseConfig(ApiKey));
                        var a = await auth.SignInWithEmailAndPasswordAsync(AuthEmail, AuthPassword);
                        //Cancellation token
                        var cancellation = new CancellationTokenSource();
                        var upload = new FirebaseStorage(Bucket,
                            new FirebaseStorageOptions
                            {
                                AuthTokenAsyncFactory = () => Task.FromResult(a.FirebaseToken),
                                ThrowOnCancel = true
                            })
                            .Child("Post")
                            .Child(""+PostID.Post_ID+"")
                            .Child($"{itemname}")
                            .PutAsync(fileStream, cancellation.Token);
                        string link = await upload;
                        await fileStream.DisposeAsync();
                        System.IO.File.Delete(FullPath);// Delete Image File in Folder Image 
                        i++;
                        statusImage = await _repository.PostImagePost(itemname,link,user_id,i,createdate);
                    }
                }
                result = await _repository.GetCreatePost(PostID.Post_ID);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            } 
            try
            {
                //status = await _repository.PutUserUploadImage(result, user_id);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (result != null)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = result;
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Data is Error";
            }
            return resData;
        }
        [HttpDelete("deletePost")]
        public async Task<ActionResult<Result>> DeletePost(int post_id)
        {
            bool status = false;
            Result resData = new Result();
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int user_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                List<ResultUploadImage> _data;
                _data = await _repository.GetImageName(post_id);
                if (_data != null)
                {
                    foreach (var item in _data)
                    {
                        string itemname = "" + item.FileName + "";
                        // Firebase uploading stuffs
                        var auth = new FirebaseAuthProvider(new FirebaseConfig(ApiKey));
                        var a = await auth.SignInWithEmailAndPasswordAsync(AuthEmail, AuthPassword);
                        //Cancellation token
                        var cancellation = new CancellationTokenSource();
                        var upload = new FirebaseStorage(Bucket,
                            new FirebaseStorageOptions
                            {
                                AuthTokenAsyncFactory = () => Task.FromResult(a.FirebaseToken),
                                ThrowOnCancel = true
                            })
                            .Child("Post/" + post_id + "")
                            .Child($"{itemname}")
                            .DeleteAsync();
                    }
                    status = await _repository.DeletePost(post_id, user_id);
                }
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (status == true)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "Delete Post is Success";
            }
            else
            {
                //await SendNotiWarning(_data.CUS_ID);
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
            }
            return resData;
        }


        [HttpGet("countfollower")]
        public async Task<ActionResult<Result>> GetCountFollower()
        {
            Result resData = new Result();
            UserFollowerCount _data = new UserFollowerCount();
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int user_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                _data = await _repository.GetFollowerCount(user_id);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (user_id == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Results = "token false";
            }
            else if (_data == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
            }
            else
            {
                //await SendNotiWarning(_data.CUS_ID);
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = _data;
            }

            return resData;
        }

        [HttpGet("countfollowing")]
        public async Task<ActionResult<Result>> GetCountFollowing()
        {
            Result resData = new Result();
            UserFollowingCount _data = new UserFollowingCount();
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int user_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                _data = await _repository.GetFollowingCount(user_id);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (user_id == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Results = "token false";
            }
            else if(_data == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
            }      
            else
            {
                //await SendNotiWarning(_data.CUS_ID);
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = _data;
            }

            return resData;
        }

        [HttpGet("countvote")]
        public async Task<ActionResult<Result>> GetCountVote()
        {
            Result resData = new Result();
            ValueVote _data = new ValueVote();
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int user_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                _data = await _repository.GetVoteCount(user_id);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (user_id == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Results = "token false";
            }
            else if (_data == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
            }
            else
            {
                //await SendNotiWarning(_data.CUS_ID);
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = _data;
            }

            return resData;
        }
        [HttpPost("follow")]
        public async Task<ActionResult<Result>> PostFollow(UserFollow data)
        {
            Result resData = new Result();
            int status = 2; // Followed
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int user_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.PostFollow(data, user_id);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            if (user_id == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Results = "token false";
            }
            else if (status == 1)
            {
                //await SendNotiWarning(_data.CUS_ID);
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = "follow success";
            }
            else if (status == 2)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "have followed";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is error";
            }

            return resData;
        }

        [HttpPost("unfollow")]
        public async Task<ActionResult<Result>> PostUnFollow(UserFollow data)
        {
            Result resData = new Result();
            int status = 2; // UnFollowed
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int user_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.PostUnFollow(data, user_id);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            if (user_id == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Results = "token false";
            }
            else if (status == 1)
            {
                //await SendNotiWarning(_data.CUS_ID);
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = "unfollow success";
            }
            else if (status == 2)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "have unfollowed";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is error";
            }

            return resData;
        }

        [HttpGet("alltag")]
        public async Task<ActionResult<Result>> GetTag(string name)
        {
            Result resData = new Result();
            List<UserTag> _data = new List<UserTag>();
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int user_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                _data = await _repository.GetAllTag(name);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (user_id == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Results = "token false";
            }
            else if (_data == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
            }
            else
            {
                //await SendNotiWarning(_data.CUS_ID);
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = _data;
            }

            return resData;
        }

        [HttpPost("postTrue/post")]
        public async Task<ActionResult<Result>> PostTrue(UserVote data)
        {
            Result resData = new Result();
            int status = 2; // Voted
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int user_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.PostTrue(data, user_id);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            if (user_id == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Results = "token false";
            }
            else if (status == 1)
            {
                //await SendNotiWarning(_data.CUS_ID);
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = "vote success";
            }
            else if (status == 2)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "have voted";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is error";
            }

            return resData;
        }
        
        [HttpPost("postNeutral/post")]
        public async Task<ActionResult<Result>> PostNeutral(UserVote data)
        {
            Result resData = new Result();
            int status = 2; // Voted
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int user_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.PostNeutral(data, user_id);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            if (user_id == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Results = "token false";
            }
            else if (status == 1)
            {
                //await SendNotiWarning(_data.CUS_ID);
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = "vote success";
            }
            else if (status == 2)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "have voted";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is error";
            }

            return resData;
        }
        
        [HttpPost("postFalse/post")]
        public async Task<ActionResult<Result>> PostFalse(UserVote data)
            {
                Result resData = new Result();
                int status = 2; // Voted
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                int user_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
                try
                {
                    status = await _repository.PostFalse(data, user_id);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
                if (user_id == 0)
                {
                    resData.StatusCode = (int)(StatusCodes.Error);
                    resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                    resData.Results = "token false";
                }
                else if (status == 1)
                {
                    //await SendNotiWarning(_data.CUS_ID);
                    resData.StatusCode = (int)(StatusCodes.Succuss);
                    resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                    resData.Results = "vote success";
                }
                else if (status == 2)
                {
                    resData.StatusCode = (int)(StatusCodes.Succuss);
                    resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                    resData.Messages = "have voted";
                }
                else
                {
                    resData.StatusCode = (int)(StatusCodes.Error);
                    resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                    resData.Messages = "data is error";
                }

                return resData;
            }

        [HttpGet("getCommentByPostID")]
        public async Task<ActionResult<Result>> GetCommentByCommentID(int Post_ID)
        {
            Result resData = new Result();
            List<UserComment> _data = new List<UserComment>();
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int user_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                _data = await _repository.GetComment(Post_ID);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (user_id == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Results = "token false";
            }
            else if (_data == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
            }
            else
            {
                //await SendNotiWarning(_data.CUS_ID);
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = _data;
            }

            return resData;
        }
       
        [HttpGet("getCountComment")]
        public async Task<ActionResult<Result>> GetCountComment(int Post_ID)
        {
            Result resData = new Result();
            UserCommentCount _data;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int user_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                _data = await _repository.GetCommentCount(Post_ID);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (user_id == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Results = "token false";
            }
            else if (_data == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
            }
            else
            {
                //await SendNotiWarning(_data.CUS_ID);
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = _data;
            }

            return resData;
        }
       
        [HttpPost("postComment")]
        public async Task<ActionResult<Result>> PostComment(UserComment data)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int user_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.PostComment(data, user_id);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Data is Error";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "Comment Success";
            }
            return resData;
        }
      
        [HttpPost("putComment")]
        public async Task<ActionResult<Result>> PutComment(UserCommentPut data)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int user_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.PutComment(data, user_id);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Data is Error";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "Put Comment Success";
            }
            return resData;
        }
       
        [HttpDelete("deleteComment")]
        public async Task<ActionResult<Result>> DeleteComment(int Comment_ID)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int user_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.DeleteComment(Comment_ID,user_id);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Data is Null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "Delete Success";
            }
            return resData;
        }
       
        [HttpGet("getCountLike/comment")]
        public async Task<ActionResult<Result>> GetCountLikeComment(int comment_id)
        {
            Result resData = new Result();
            UserLikeCount _data;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int user_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                _data = await _repository.GetLikeCommentCount(comment_id);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            if (user_id == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Results = "token false";
            }
            else if (_data != null)
            {
                //await SendNotiWarning(_data.CUS_ID);
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = _data;
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is error";
            }

            return resData;
        }
      
        [HttpPost("like/comment")]
        public async Task<ActionResult<Result>> PostLikeComment(UserLikeComment data)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int user_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.PostLikeComment(data, user_id);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            if (user_id == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Results = "token false";
            }
            else if (status == true)
            {
                //await SendNotiWarning(_data.CUS_ID);
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = "like comment success";
            }
            else if (status == false)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "haved like this comment";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is error";
            }

            return resData;
        }
      
        [HttpPost("unlike/comment")]
        public async Task<ActionResult<Result>> PostUnLikeComment(UserLikeComment data)
        {
            Result resData = new Result();
            bool status = false;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int user_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.PostUnLikeComment(data, user_id);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            if (user_id == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Results = "token false";
            }
            else if (status == true)
            {
                //await SendNotiWarning(_data.CUS_ID);
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = "unlike comment success";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is error";
            }

            return resData;
        }
      
        [HttpGet("getLike/comment")]
        public async Task<ActionResult<Result>> GetLike(int Post_ID)
        {
            Result resData = new Result();
            List<UserLike> _data = new List<UserLike>();
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int user_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);

            try
            {
                _data = await _repository.GetLike(Post_ID);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (user_id == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Results = "token false";
            }
            else if (_data == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
            }
            else
            {
                //await SendNotiWarning(_data.CUS_ID);
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = _data;
            }

            return resData;
        }
       
        [HttpPost("like/post")]
        public async Task<ActionResult<Result>> PostLike(UserLike data)
        {
            Result resData = new Result();
            int status = 2; // Followed
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int user_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.PostLike(data, user_id);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            if (user_id == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Results = "token false";
            }
            else if (status == 1)
            {
                //await SendNotiWarning(_data.CUS_ID);
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = "like success";
            }
            else if (status == 2)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "have liked";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is error";
            }

            return resData;
        }

        [HttpPost("unlike/post")]
        public async Task<ActionResult<Result>> PostUnLike(UserLike data)
        {
            Result resData = new Result();
            int status = 2; // UnFollowed
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int user_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.PostUnLike(data, user_id);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            if (user_id == 0)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Results = "token false";
            }
            else if (status == 1)
            {
                //await SendNotiWarning(_data.CUS_ID);
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = "unlike success";
            }
            else if (status == 2)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "have unliked";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is error";
            }

            return resData;
        }
     
        [HttpGet("getMyBookmark")]
        public async Task<ActionResult<Result>> GetMyBookmark()
        {
            Result resData = new Result();
            List<UserBookmark> _data;
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            int user_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                _data = await _repository.GetMyBookmark(user_id);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (_data == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Data is Null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = _data;
            }
            return resData;
        }
      
        [HttpPost("addBookmark")]
        public async Task<ActionResult<Result>> PostBookmark(UserBookmark _data)
        {
            Result resData = new Result();
            bool status = false;
            var identiry = HttpContext.User.Identity as ClaimsIdentity;
            int user_id = int.Parse(identiry.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.PostBookmark(_data, user_id);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (status == true)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "add bookmark success";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            return resData;
        }

        [HttpPost("removeBookmark")]
        public async Task<ActionResult<Result>> PostUnBookmark(UserBookmark _data)
        {
            Result resData = new Result();
            bool status = false;
            var identiry = HttpContext.User.Identity as ClaimsIdentity;
            int user_id = int.Parse(identiry.FindFirst(ClaimTypes.NameIdentifier).Value);
            try
            {
                status = await _repository.PostUnBookmark(_data, user_id);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (status == true)
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Messages = "remove bookmark success";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "data is null";
            }
            return resData;
		}

        [HttpGet("FeedExplore")]
        public async Task<ActionResult<Result>> FeedExplore(string type)
        {
            Result resData = new Result();
            List<Explore> _data;
            //var identity = HttpContext.User.Identity as ClaimsIdentity;
            //int user_id = int.Parse(identity.FindFirst(ClaimTypes.NameIdentifier).Value);
           
            try
            {
                _data = await _repository.GetExplore(type);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            if (_data == null)
            {
                resData.StatusCode = (int)(StatusCodes.Error);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Error);
                resData.Messages = "Data is Null";
            }
            else
            {
                resData.StatusCode = (int)(StatusCodes.Succuss);
                resData.StatusMessages = (String)EnumString.GetStringValue(StatusCodes.Succuss);
                resData.Results = _data;
            }
            return resData;
        }


    }
}
