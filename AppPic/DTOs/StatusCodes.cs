﻿using Pic.DTOs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace AppPic.DTOs
{
    public class Result
    {
        public int StatusCode { get; set; }
        public string StatusMessages { get; set; }
        public string Messages { get; set; }
        public object Results { get; set; }
    }
    //public class ResultUserLogin
    //{
    //    public int StatusCode { get; set; }
    //    public string StatusMessages { get; set; }
    //    public string Messages { get; set; }
    //    public object Results { get; set; }
    //}
    public class ResultUploadImage
    {
        public string FileName { get; set; }
        public string ImagePath { get; set; }
    }
    public class ResultCreatePost
    {
        public string post_id { get; internal set; }
        public string Message { get; set; }
        public open_vote Open_Vote { get; set; }
        public string Schedule { get; set; }
        public List<Image> Images { get; set; }
        
    }
    public class Image
    {
        public int Order { get; set; }
        public string Image_url { get; set; }
    }

    public enum StatusCodes
    {
        [EnumValue("Connection Error.")]
        Connection = 99,
        [EnumValue("Success.")]
        Succuss = 1,
        [EnumValue("Error.")]
        Error = 0
    }

    public enum StatusVote
    {
        [EnumValue("Neutral")]
        Succuss = 2,
        [EnumValue("True")]
        Connection = 1,
        [EnumValue("False")]
        Error = 0
    }

}
