﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppPic.DTOs
{
    public class EmailForgotPassword
    {
        public string topic { get; set; }
        public string title1 { get; set; }
        public string title2 { get; set; }
        public EmailForgotPasswordBotton button { get; set; }
        public EmailForgotPasswordCustomerInfo customerInfo { get; set; }
        public string title3 { get; set; }
        public EmailFrom from { get; set; }
        public EmailFooter footer { get; set; }
    }

    public class EmailForgotPasswordBotton
    {
        public string otp { get; set; }
    }

    public class EmailForgotPasswordCustomerInfo
    {
        public string title { get; set; }
        public string name { get; set; }
    }

    public class EmailWelcomeRestaurant
    {
        public string topic { get; set; }
        public string title1 { get; set; }
        public string title2 { get; set; }
        public string title3 { get; set; }
        public string title4 { get; set; }
        public EmailFrom from { get; set; }
        public EmailFooter footer { get; set; }
    }

    public class EmailWelcomeCustomer
    {
        public string topic { get; set; }
        public string title1 { get; set; }
        public string title2 { get; set; }
        public string title3 { get; set; }
        public string title4 { get; set; }
        public EmailWelcomeCustomerBotton button { get; set; }
        public string title5 { get; set; }
        public EmailFrom from { get; set; }
        public EmailFooter footer { get; set; }
    }

    public class EmailWelcomeCustomerBotton
    {
        public string url { get; set; }
        public string title { get; set; }
    }

    public class EmailOrderRecept
    {
        public string topic { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public EmailOrderReceptHeader order { get; set; }
        public EmailOrderReceptDetail receipt { get; set; }
        public EmailOrderReceptPayment payment { get; set; }
        public EmailOrderReceptCustomer customer { get; set; }
        public EmailOrderReceptApplication application { get; set; }
        public EmailOrderReceptAction action { get; set; }
        public EmailFooter footer { get; set; }
    }

    public class EmailOrderReceptHeader
    {
        public string title { get; set; }
        public string number { get; set; }
    }

    public class EmailOrderReceptDetail
    {
        public string title { get; set; }
        public List<EmailOrderReceptDetailItem> orders { get; set; }
        public EmailOrderReceptSummary summary { get; set; }
        public EmailHelp getOrderHelp { get; set; }

    }

    public class EmailOrderReceptDetailItem
    {
        public int amount { get; set; }
        public string name { get; set; }
        public int price { get; set; }
        public EmailOrderReceptDetailItemComment comment { get; set; }
        public EmailOrderReceptDetailItemAddon addons { get; set; }
    }

    public class EmailOrderReceptDetailItemComment
    {
        public bool hasComment { get; set; }
        public string title { get; set; }
        public string comment { get; set; }
    }

    public class EmailOrderReceptDetailItemAddon
    {
        public bool hasAddons { get; set; }
        public string title { get; set; }
        public List<EmailOrderReceptDetailItemAddonList> list { get; set; }
    }

    public class EmailOrderReceptDetailItemAddonList
    {
        public string name { get; set; }
        public int price { get; set; }
    }

    public class EmailOrderReceptSummary
    {
        public EmailOrderReceptSummaryDetail subTotal { get; set; }
        public EmailOrderReceptSummaryDetail tax { get; set; }
        public EmailOrderReceptSummaryDetail deliveryFee { get; set; }
        public EmailOrderReceptSummaryDetail transactionFee { get; set; }
        public EmailOrderReceptSummaryDetail totalCharged { get; set; }
    }

    public class EmailOrderReceptSummaryDetail
    {
        public string title { get; set; }
        public int amount { get; set; }
    }

    public class EmailOrderReceptPayment
    {
        public string title { get; set; }
        public string method { get; set; }
        public string cardNumber { get; set; }
        public int amount { get; set; }
    }

    public class EmailOrderReceptCustomer
    {
        public string address { get; set; }
        public string For { get; set; }
        public string name { get; set; }
    }

    public class EmailOrderReceptApplication
    {
        public string title { get; set; }
        public string appStore { get; set; }
        public string playStore { get; set; }
    }

    public class EmailOrderReceptAction
    {
        public EmailHelp helpCenter { get; set; }
        public EmailHelp referral { get; set; }
        public EmailHelp dasher { get; set; }
    }

    public class EmailHelp
    {
        public string url { get; set; }
        public string title { get; set; }
    }

    public class EmailFrom
    {
        public string title { get; set; }
        public string name { get; set; }
    }

    public class EmailFooter
    {
        public string address { get; set; }
        public string webUrl { get; set; }
        public string facebookUrl { get; set; }
        public string instagramUrl { get; set; }
    }
}
