﻿using AppPic.DTOs;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pic.DTOs
{
    public class UserResetPasswordToEmail
    {
        public string Email { get; set; }

    }

    public class UserPasswordReset
    {
        public int User_ID { get; set; }
        public string Email { get; set; }
        public string Token_Key { get; set; }
        public string Verify_Code { get; set; }
        public int Status { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ExpireDate { get; set; }
    }

    public class UserSignUp
    {
        //public int User_ID { get; set; }
        public string Email { get; set; }
        public string Token_Key { get; set; }
        public string Verify_Code { get; set; }
        public int Status { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ExpireDate { get; set; }
    }

    public class UserLogin
    {
        public string Email { get; set; }
        //public string Username { get; set; }
        public string Password { get; set; }
    }
    public class UserInfo
    {
        public int User_ID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }
        public string PhoneNumber { get; set; }
        public string Username { get; set; }
        //public string Password { get; set; }
        public string Image { get; set; }
        //public string ImagePath { get; set; }
    }

    public class UploadProfileImage
    {
        public IFormFile Image { get; set; }
    }
    public class UploadProfileImages
    {
        public IFormFileCollection Image { get; set; }
    }
    public class UserUpdate
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Username { get; set; }

    }
    public class UsernameCheck
    {
        public string Username { get; set; }

    }
    public class PostID
    {
        public int Post_ID { get; set; }
    }

    public class UserEmail
    {
        public string Email { get; set; }

    }

    public class UserSignup
    {
        public string Email { get; set; }
        //public string PhoneNumber { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

    }
    public class UserPasswordForget
    {
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Password { get; set; }
    }

    public class UserPassword
    {
        public string CurrentPassword { get; set; }
        public string New_Password { get; set; }
    }

    public class UserName
    {
        public int User_ID { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
    }

    public class UserFollowing
    {
        public string UserFollowTo_Username { get; set; }

    }
    public class UserFollowingCount
    {
        public int values { get; set; }

    }
    public class FollowerUser
    {
        public string UserFollowBy_Username { get; set; }
    }
    public class UserFollowerCount
    {
        public int values { get; set; }
    }

    public class UserFollow
    {
        public int UserFollowBy_ID { get; internal set; }
       // public string UserFollowBy_Username { get; set; }
        public int UserFollowTo_ID { get; set; }
        //public string UserFollowTo_Username { get; set; }
    }

    public class UserTag
    {
        public string TagName { get; set; }
        public int TagValue { get; set; }
    }

    public class UserVote
    {
        public int Vote_ID { get; internal set; }
        public int User_ID { get; internal  set; }
        public int Post_ID { get; set; }
        public string UserVote_Username { get; internal set; }
        public int VoteType { get; internal set; }
    }

    public class ValueVote
    {
        public int value1 { get; set; }
        public List<UserName> UsernameVote1 { get; set; }
        public int value2 { get; set; }
        public List<UserName> UsernameVote2 { get; set; }

        public int value3 { get; set; }
        public List<UserName> UsernameVote3 { get; set; }

    }

    //public class UserVoteCount
    //{
    //    public int values { get; set; }
    //}


    public class UserComment
    {
        public int Post_ID { internal get; set; }
        public string Comments { get; set; }
        public DateTime CreateDate { get; internal set; }
    }
    public class UserLikeComment
    {
        public int Comment_ID { get; set; } 
    }
    public class UserCommentPut
    {
        public int Comment_ID { get; set; }
        public int Post_ID { internal get; set; }
        public string Comments { get; set; }
        public DateTime CreateDate { get; internal set; }
    }

    public class UserCommentCount
    {
        public int values { get; set; }
        // public List<UserComment> userCommentLists { get; set; }

    }

    public class UserLike
    {
        public int UserLike_ID {  get; internal set; }
        public string UserLike_Username { get; internal set; }
        public int Post_ID { internal get; set; }
        public DateTime CreateDate { get; internal set; }
    }

    public class UserLikeCount
    {
        public int values { get; set; }
        //public List<UserLike> userLikes { get; set; }
    }

    public class UserBookmark
    {
        public int BookmarkBy_ID { get;  internal set; }
        public string BookmarkBy_Username { get; internal set; }
        public int Post_ID { get; set; }
        public List<Image> Images { get; internal set; }
    }

    public class UserPost
    {
        public string Message { get; set; }
        public open_vote Open_Vote { get; set; }
        public string Schedule { get; set; }
    }
    public class open_vote
    {
        public int Day { get; set; }
        public int Second { get; set; }
    }


    public class Explore
    {
        public string ImagePath { get; set; }
    }
}
