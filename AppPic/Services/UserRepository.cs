﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Nancy;
using Pic.DTOs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using AppPic.DTOs;

namespace Pic.Services
{
    public interface IUserData
    {
        // METHOD GET //
        Task<UserInfo> GetLogin(UserLogin data);
        Task<UserInfo> GetProfile(int user_id);
        Task<bool> CheckUserHaveInDatabase(int user_id);
        Task<PostID> CheckPostHaveInDatabase(int user_id, DateTime createdate);
        Task<List<UserName>> GetAllUsername(int user_id,string name);
        Task<UserName> GetUserByID(int user_id);
        Task<List<UserTag>> GetAllTag(string name);
        Task<List<FollowerUser>> GetFollower(int user_id);
        Task<UserFollowerCount> GetFollowerCount(int user_id);
        Task<List<UserFollowing>> GetFollowing(int user_id);
        Task<UserFollowingCount> GetFollowingCount(int user_id);
        Task<List<UserVote>> GetVote(int post_id);
        Task<ValueVote> GetVoteCount(int post_id);
        Task<List<UserLike>> GetLike(int post_id);
        Task<UserLikeCount> GetLikeCount(int post_id);
        Task<UserLikeCount> GetLikeCommentCount(int comment_id);
        Task<List<UserComment>> GetComment(int post_id);
        Task<UserCommentCount> GetCommentCount(int post_id);
        Task<UserInfo> GetCustomerByPhoneOrEmail(string EmailOrPhone);
        Task<List<ResultCreatePost>> GetMyPost(int user_id);
        Task<ResultCreatePost> GetCreatePost(int post_id);
        Task<List<ResultUploadImage>> GetImageName(int post_id);
        Task<List<UserBookmark>> GetMyBookmark(int user_id);
        Task<List<Explore>> GetExplore(string type);

        // METHOD POST //

        Task<bool> PostUserLog(UserInfo data, string guid);
        Task<int> PostUserRegister(UserSignup data);
        Task<bool> PostPasswordChange(UserPassword data, int user_id);
        Task<int> PostFollow(UserFollow data, int user_id);
        Task<int> PostUnFollow(UserFollow data, int user_id);
        Task<int> PostTrue(UserVote data, int user_id);
        Task<int> PostNeutral(UserVote data, int user_id);
        Task<int> PostFalse(UserVote data, int user_id);
        Task<bool> PostComment(UserComment data, int user_id);
        Task<bool> PostLikeComment(UserLikeComment data, int user_id);
        Task<bool> PostUnLikeComment(UserLikeComment data, int user_id);
        Task<int> PostLike(UserLike data, int Post_id);
        Task<int> PostUnLike(UserLike data, int Post_id);
        Task<int> PostUsernameCheck(UsernameCheck _data);
        Task<int> PostEmailCheck(UserEmail data);
        Task<bool> PostBookmark(UserBookmark data, int user_id);
        Task<bool> PostUnBookmark(UserBookmark data, int user_id);
        Task<bool> GetSendOTPPasswordToEmail(UserPasswordReset data);
        Task<int> PostCheckOTPPasswordFromWithEmail(CheckOtpResetPwWithEmail data);
        Task<int> PostCheckOTPPasswordFromWithOutEmail(CheckOtpResetPwWithOutEmail data);
        Task<bool> GetSendOTPSignUpToEmail(UserSignUp data);
        Task<int> PostCheckOTPSignUpFromWithEmail(CheckOtpSignUpWithEmail data);
        Task<int> PostCheckOTPSignUpFromWithOutEmail(CheckOtpSignUpWithOutEmail data);
        Task<bool> CreatePost(UserPost _data, int user_id, DateTime createdate);
        Task<bool> PostImagePost(string FileName, string link, int user_id, int i, DateTime createdate);

        // METHOD PATCH //

        // METHOD PUT //
        Task<bool> PutUserUploadImage(ResultUploadImage _data, int user_id);
        Task<int> PutProfile(UserUpdate data, int user_id);
        Task<bool> PutComment(UserCommentPut data, int user_id);


        // METHOD DELETE //
        Task<bool> DeleteComment(int Comment_id, int user_id);
        Task<bool> DeletePost(int post_id,int user_id);
    }
    public class UserRepository : IUserData
    {
        private readonly IConfiguration configuration;

        public UserRepository(IConfiguration config)
        {
            this.configuration = config;
        }

        public IDbConnection Connection
        {
            get
            {
                return new SqlConnection(ConfigurationExtensions.GetConnectionString(this.configuration, "DefaultConnection"));
            }
        }

        // METHOD GET //
        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {

                return false;
            }
        }

        public async Task<UserInfo> GetLogin(UserLogin data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    string email = "";
                    //string username = "";

                    if (IsValidEmail(data.Email) == true)
                    {
                        email = data.Email;
                        //username = null;
                    }
                    else
                    {
                        email = null;
                        //username = data.Username;
                    }

                    var result = await dbConnection.QueryFirstOrDefaultAsync<UserInfo>("SELECT User_ID, Name, Email, Token, PhoneNumber, Username, Image FROM [dbo].[User] WHERE Email=@EMAIL AND Password=@PASSWORD COLLATE SQL_Latin1_General_CP1_CS_AS AND IsEnable = 1", new { EMAIL = email, PASSWORD = data.Password });

                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<UserInfo> GetProfile(int user_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {

                    var result = await dbConnection.QueryFirstOrDefaultAsync<UserInfo>("SELECT User_ID, Name, Email, Token, PhoneNumber, Username, Image FROM [dbo].[User] WHERE User_ID =@USERID AND IsEnable = 1", new { USERID = user_id });

                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<bool> CheckUserHaveInDatabase(int user_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {

                    int checkName = await dbConnection.QueryFirstOrDefaultAsync<int>("SELECT User_ID FROM [dbo].[User] WHERE User_ID =@USERID AND IsEnable =1", new { USERID = user_id });

                    if (checkName > 0)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<PostID> CheckPostHaveInDatabase(int user_id, DateTime createdate)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    var post_id = await dbConnection.QueryFirstOrDefaultAsync<PostID>("SELECT Post_ID FROM [dbo].[Post] WHERE User_ID = @USERID and  IsEnable = 1 and CreateDate = @DATE", new { USERID = user_id, DATE = createdate });
                    return post_id;
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        public async Task<List<UserName>> GetAllUsername(int user_id,string name)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    IEnumerable<UserName> result;
                    if (name != null)
                    {
                        result = await dbConnection.QueryAsync<UserName>("SELECT Distinct Username,User_ID,Name,Image FROM [dbo].[User] WHERE Username LIKE '" + name + "%'  and IsEnable = 1 ORDER BY Username", new { NAME = name });
                    }
                    else
                    {
                        result = await dbConnection.QueryAsync<UserName>("SELECT Distinct Username,User_ID,Name,Image FROM [dbo].[User] WHERE IsEnable = 1 ORDER BY Username");
                    }
                    return result.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<UserName> GetUserByID(int user_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var result = await dbConnection.QueryFirstOrDefaultAsync<UserName>("SELECT User_ID,Username,Name,Image FROM [dbo].[User] WHERE User_ID = @USERID", new { USERID = user_id });
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<UserTag>> GetAllTag(string name)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    IEnumerable<UserTag> result;
                    if (name != null)
                    {
                        result = await dbConnection.QueryAsync<UserTag>("SELECT A.Name as 'tagName',COUNT(B.LogTag_ID) as 'tagValue'  FROM [dbo].[Tag] as A INNER JOIN [dbo].[TagLog] as B ON B.Tag_ID = A.Tag_ID  WHERE A.Name LIKE '"+ name +"%' and B.IsEnable = 1 GROUP BY A.Name ORDER BY A.Name", new { NAME = name });
                    }
                    else
                    {
                        result = await dbConnection.QueryAsync<UserTag>("SELECT A.Name as 'tagName',COUNT(B.LogTag_ID) as 'tagValue'  FROM [dbo].[Tag] as A INNER JOIN [dbo].[TagLog] as B ON B.Tag_ID = A.Tag_ID  WHERE B.IsEnable = 1 GROUP BY A.Name ORDER BY A.Name");
                    }

                    return result.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<UserVote>> GetVote(int post_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var result = await dbConnection.QueryAsync<UserVote>("SELECT UserLike_ID, UserLike_Username, Post_ID, CreateDate FROM [Like] WHERE Post_ID = @POSTID and IsEnable = 1", new { POSTID = post_id });
                    return result.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<ValueVote> GetVoteCount(int user_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {

                    var result = await dbConnection.QueryFirstOrDefaultAsync<ValueVote>("SELECT dbo.[fnCheckVoteCount](@USERID,1) as value1,dbo.[fnCheckVoteCount](@USERID,2) as value2, dbo.[fnCheckVoteCount](@USERID,0) as value3 FROM [dbo].[Vote] WHERE [User_ID] = @USERID AND [IsEnable] = 1", new { USERID = user_id });
                    var userName1 = await dbConnection.QueryAsync<UserName>("SELECT Distinct B.User_ID,B.Username FROM Vote as A  INNER JOIN [dbo].[User] as B ON B.User_ID = A.User_ID WHERE VoteType = 1 AND B.IsEnable = 1", new { USERID = user_id });
                    result.UsernameVote1 = userName1.ToList();
                    var userName2 = await dbConnection.QueryAsync<UserName>("SELECT Distinct B.User_ID,B.Username FROM Vote as A  INNER JOIN [dbo].[User] as B ON B.User_ID = A.User_ID WHERE VoteType = 2 AND B.IsEnable = 1", new { USERID = user_id });
                    result.UsernameVote2 = userName2.ToList();
                    var userName3 = await dbConnection.QueryAsync<UserName>("SELECT Distinct B.User_ID,B.Username FROM Vote as A  INNER JOIN [dbo].[User] as B ON B.User_ID = A.User_ID WHERE VoteType = 0 AND B.IsEnable = 1", new { USERID = user_id });
                    result.UsernameVote3 = userName3.ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<FollowerUser>> GetFollower(int user_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var result = await dbConnection.QueryAsync<FollowerUser>("SELECT DISTINCT [dbo].[User].[Username] AS UserFollowBy_Username FROM [dbo].[Follow] INNER JOIN  [dbo].[User] ON [dbo].[Follow].[User_ID] = [dbo].[User].[User_ID] WHERE [dbo].[Follow].[UserFollowTo_ID] = @USERID AND [dbo].[Follow].[IsEnable] = 1", new { USERID = user_id });
                    return result.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<UserFollowerCount> GetFollowerCount(int user_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {

                    var result = await dbConnection.QueryFirstOrDefaultAsync<UserFollowerCount>("SELECT COUNT([UserFollowBy_ID]) AS [values]  FROM [dbo].[Follow] WHERE [UserFollowTo_ID] = @USERID AND [IsEnable] = 1", new { USERID = user_id });
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<UserFollowing>> GetFollowing(int user_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var result = await dbConnection.QueryAsync<UserFollowing>("SELECT DISTINCT [dbo].[User].[Username] AS UserFollowTo_Username FROM [dbo].[Follow] INNER JOIN  [dbo].[User] ON [dbo].[Follow].[User_ID] = [dbo].[User].[User_ID] WHERE [dbo].[Follow].[UserFollowBy_ID] = @USERID AND [dbo].[Follow].[IsEnable] = 1", new { USERID = user_id });
                    return result.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<UserFollowingCount> GetFollowingCount(int user_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var result = await dbConnection.QueryFirstOrDefaultAsync<UserFollowingCount>("SELECT COUNT([UserFollowTo_ID]) AS [values] FROM [dbo].[Follow] WHERE [UserFollowBy_ID] = @USERID AND [IsEnable] = 1", new { USERID = user_id });
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<UserLike>> GetLike(int post_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var result = await dbConnection.QueryAsync<UserLike>("SELECT UserLike_ID, UserLike_Username, Post_ID, CreateDate FROM [Like] WHERE Post_ID = @POSTID and IsEnable = 1", new { POSTID = post_id });
                    return result.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<UserLikeCount> GetLikeCount(int post_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var result = await dbConnection.QueryFirstOrDefaultAsync<UserLikeCount>("SELECT Count(Post_ID) as [values] FROM [dbo].[Like] WHERE Post_ID = @POSTID and IsEnable = 1", new { POSTID = post_id });

                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<UserLikeCount> GetLikeCommentCount(int comment_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var result = await dbConnection.QueryFirstOrDefaultAsync<UserLikeCount>("SELECT Count(Comment_ID) as [values] FROM [dbo].[LikeComment] WHERE Comment_ID = @COMMENTID and IsEnable = 1", new { COMMENTID = comment_id });

                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<UserComment>> GetComment(int post_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var comment = await dbConnection.QueryAsync<UserComment>("SELECT UserComment_Username, Comment, CreateDate FROM Comment WHERE Post_ID = @POSTID and IsEnable = 1 ORDER BY CreateDate", new { POSTID = post_id });
                    return comment.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<UserCommentCount> GetCommentCount(int post_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var result = await dbConnection.QueryFirstOrDefaultAsync<UserCommentCount>("SELECT Count(Comment_ID) as [values] FROM Comment WHERE Post_ID = @POSTID and IsEnable = 1", new { POSTID = post_id });
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<UserInfo> GetCustomerByPhoneOrEmail(string EmailOrPhone)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    string email = "";
                    string phone = "";
                    if (IsValidEmail(EmailOrPhone) == true)
                    {
                        email = EmailOrPhone;
                        phone = null;
                    }
                    else
                    {
                        email = null;
                        phone = EmailOrPhone;
                    }
                    var results = await dbConnection.QueryFirstOrDefaultAsync<UserInfo>("SELECT User_ID, Name, Email, Token, PhoneNumber, Username, Image FROM [dbo].[User] WHERE Email=@EMAIL OR PhoneNumber=@PHONE", new { EMAIL = email, PHONE = phone });
                    return results;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<Image>> GetImagePost(int post_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var _dataImage = await dbConnection.QueryAsync<Image>("SELECT [ImagePath] as 'Image_url',[Sequence] as 'Order' FROM [dbo].[PostImage] WHERE Post_ID = @POSTID  ORDER BY [Sequence]", new { POSTID = post_id });
                    return _dataImage.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<ResultCreatePost>> GetMyPost(int user_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var _data = await dbConnection.QueryAsync<ResultCreatePost>("SELECT * FROM [dbo].[Post] WHERE [User_ID] = @USERID",new { USERID = user_id });
                    foreach (var item in _data)
                    {
                        var _dataimages = await dbConnection.QueryAsync<Image>("SELECT Sequence as 'Order',ImagePath as 'Image_url' FROM [dbo].[Post] as A  INNER JOIN [dbo].[PostImage] as B ON B.Post_ID = A.Post_ID WHERE B.Post_ID = @POSTID AND A.[User_ID] = @USERID",new { POSTID = item.post_id , USERID = user_id});
                        item.Images = _dataimages.ToList();
                    }
                    return _data.ToList();
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        public async Task<ResultCreatePost> GetCreatePost(int post_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var _dataImage = await dbConnection.QueryFirstOrDefaultAsync<ResultCreatePost>("SELECT * FROM [dbo].[Post] WHERE  Post_ID = @POSTID and IsEnable = 1", new { POSTID = post_id });
                    _dataImage.Open_Vote = await dbConnection.QueryFirstAsync<open_vote>("SELECT Day,Second FROM [dbo].[Post] WHERE  Post_ID = @POSTID and IsEnable = 1", new { POSTID = post_id });
                    var Images = await dbConnection.QueryAsync<Image>("SELECT [ImagePath] as 'Image_url',[Sequence] as 'Order' FROM [dbo].[PostImage] WHERE Post_ID = @POSTID  ORDER BY [Sequence]", new { POSTID = post_id });
                    _dataImage.Images = Images.ToList();
                    //= await dbConnection.QueryAsync<Image>("SELECT [ImagePath] as 'Image_url',[Sequence] as 'Order' FROM [dbo].[PostImage] WHERE Post_ID = @POSTID  ORDER BY [Sequence]", new { POSTID = post_id });
                    return _dataImage;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<List<ResultUploadImage>> GetImageName(int post_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var _data = await dbConnection.QueryAsync<ResultUploadImage>("SELECT FileName FROM [dbo].[PostImage] WHERE Post_ID = @POSTID", new { POSTID = post_id });
                    return _data.ToList();
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        public async Task<List<UserBookmark>> GetMyBookmark(int user_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var _data = await dbConnection.QueryAsync<UserBookmark>("SELECT * FROM [dbo].[Bookmark] WHERE BookmarkBy_ID = @USERID",new { USERID = user_id});
                    foreach (var item in _data)
                    {
                        var Images = await dbConnection.QueryAsync<Image>("SELECT [ImagePath] as 'Image_url',[Sequence] as 'Order'  FROM [dbo].[Bookmark] as A INNER JOIN [dbo].[PostImage] as B  ON B.Post_ID = A.Post_ID WHERE A.BookmarkBy_ID = @USERID and A.Post_ID = @POSTID", new { USERID = user_id, POSTID = item.Post_ID });
                        item.Images = Images.ToList();
                    }
                    return _data.ToList();
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        // METHOD POST //
        public async Task<bool> PostUserLog(UserInfo data, string guid)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int status = await dbConnection.ExecuteAsync("INSERT INTO [dbo].[UserLogin] (User_ID,Guid) VALUES (@USERID,@GUID)", new { USERID = data.User_ID, GUID = guid });

                    if (status == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<int> PostUserRegister(UserSignup data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int checkName = await dbConnection.QueryFirstOrDefaultAsync<int>("SELECT User_ID FROM [dbo].[User] WHERE (Email=@EMAIL OR PhoneNumber=@USERNAME) AND Email != '' and Username != ''", new { EMAIL = data.Email, USERNAME = data.Username });

                    if (checkName > 0)
                        return 0;
                    else
                    {
                        int status = await dbConnection.ExecuteAsync("INSERT INTO [dbo].[User] (Email,Username,Password,IsEnable) VALUES " +
                        "(@EMAIL,@USERNAME,@PASSWORD,1)", new
                        { EMAIL = data.Email, USERNAME = data.Username, PASSWORD = data.Password });
                        if (status == 1)
                            return 1;
                        else
                            return 2;
                    }

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<bool> PostPasswordChange(UserPassword data, int user_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int checkpassword = await dbConnection.QueryFirstOrDefaultAsync<int>("SELECT * FROM [dbo].[User] WHERE User_ID = @USERID and Password = @PASSWORD", new { USERID = user_id, PASSWORD = data.CurrentPassword });
                    if (checkpassword > 0)
                    {
                        int status = await dbConnection.ExecuteAsync("UPDATE [dbo].[User] SET Password = @NEWPASSWORD WHERE User_ID = @USERID", new { NEWPASSWORD = data.New_Password, USERID = user_id });
                        if (status == 1)
                            return true;
                        else
                            return false;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> PostFollow(UserFollow data, int user_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int _count = await dbConnection.QueryFirstOrDefaultAsync<int>("  SELECT LogFollow_ID FROM [dbo].[Follow] WHERE UserFollowBy_ID = @USERFOLLOWBYID AND UserFollowTo_ID = @USERFOLLOWTOID AND IsEnable = 0", new { USERFOLLOWBYID = user_id, USERFOLLOWTOID = data.UserFollowTo_ID });

                    if (_count > 0)
                    {
                        int status = await dbConnection.ExecuteAsync("UPDATE [dbo].[Follow] SET IsEnable = 1,FollowAgainDate = @DATE WHERE UserFollowBy_ID =@USERFOLLOWBYID AND UserFollowTo_ID =@USERFOLLOWTOID", new
                        { DATE = DateTime.Now, USERFOLLOWBYID = user_id, USERFOLLOWTOID = data.UserFollowTo_ID });

                        if (status == 1)
                            return 1;
                        else
                            return 0;
                    }
                    else
                    {
                        var _data = await dbConnection.QueryFirstOrDefaultAsync<UserInfo>("SELECT Username FROM [dbo].[User] WHERE User_ID = @USERID", new { USERID = user_id });
                        var _dataTo = await dbConnection.QueryFirstOrDefaultAsync<UserInfo>("SELECT Username FROM [dbo].[User] WHERE User_ID = @USERID", new { USERID = data.UserFollowTo_ID });
                        int status = await dbConnection.ExecuteAsync("INSERT INTO [dbo].[Follow] (UserFollowBy_ID,UserFollowBy_Username,UserFollowTo_ID,UserFollowTo_Username,IsEnable) VALUES(@USERFOLLOWBYID,@USERFOLLOWBYUSERNAME,@USERFOLLOWTOID,@USERFOLLOWTOUSERNAME,1)", new
                        { USERFOLLOWBYID = user_id, USERFOLLOWBYUSERNAME = _data.Username, USERFOLLOWTOID = data.UserFollowTo_ID, USERFOLLOWTOUSERNAME = _dataTo.Username });
                        if (status == 1)
                            return 1;
                        else
                            return 0;

                        //int status = await dbConnection.ExecuteAsync("INSERT INTO [dbo].[Follow] (UserFollowBy_ID,UserFollowBy_Username,UserFollowTo_ID,UserFollowTo_Username,IsEnable) VALUES " +
                        //"(@USERFOLLOWBYID,@USERFOLLOWBYUSERNAME,@USERFOLLOWTOID,@USERFOLLOWTOUSERNAME,1)", new
                        //{ USERFOLLOWBYID = user_id, USERFOLLOWBYUSERNAME = data.UserFollowBy_Username, USERFOLLOWTOID = data.UserFollowTo_ID, USERFOLLOWTOUSERNAME = data.UserFollowTo_Username });
                        //if (status == 1)
                        //    return 1;
                        //else
                        //    return 0;
                    }

                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<int> PostUnFollow(UserFollow data, int user_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int _count = await dbConnection.QueryFirstOrDefaultAsync<int>("SELECT LogFollow_ID FROM [dbo].[Follow] WHERE UserFollowBy_ID =@USERFOLLOWBYID AND UserFollowTo_ID =@USERFOLLOWTOID AND IsEnable = 1", new { USERFOLLOWBYID = user_id, USERFOLLOWTOID = data.UserFollowTo_ID });

                    int _result = 0;

                    if (_count > 0)
                    {
                        int status = await dbConnection.ExecuteAsync("UPDATE [dbo].[Follow] SET IsEnable = 0,UnfollowDate = @DATE WHERE UserFollowBy_ID =@USERFOLLOWBYID AND UserFollowTo_ID =@USERFOLLOWTOID", new
                        { DATE = DateTime.Now, USERFOLLOWBYID = user_id, USERFOLLOWTOID = data.UserFollowTo_ID });
                        if (status == 1)
                            _result = 1;
                        else
                            _result = 0;
                    }

                    return _result;
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        public async Task<int> PostTrue(UserVote data, int user_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int checkName = await dbConnection.QueryFirstOrDefaultAsync<int>("SELECT User_ID FROM [dbo].[Vote] WHERE User_ID =  @USERID and Post_ID = @POSTID and IsEnable =1", new { USERID = user_id, POSTID = data.Post_ID });
                    if (checkName > 0)
                        return 2;
                    else
                    {
                        var _data = await dbConnection.QueryFirstOrDefaultAsync<UserInfo>("SELECT Username FROM [dbo].[User] WHERE User_ID = @USERID and IsEnable = 1", new { USERID = user_id });
                        int status = await dbConnection.ExecuteAsync("INSERT INTO [dbo].[Vote] (User_ID,Username,Post_ID,VoteType,IsEnable) VALUES " +
                        "(@USERID,@USERNAME,@POSTID,1,1)", new
                        { USERID = user_id, USERNAME = _data.Username, POSTID = data.Post_ID });
                        if (status == 1)
                            return 1;
                        else
                            return 0;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<int> PostNeutral(UserVote data, int user_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int checkName = await dbConnection.QueryFirstOrDefaultAsync<int>("SELECT User_ID FROM [dbo].[Vote] WHERE User_ID =  @USERID and Post_ID = @POSTID and IsEnable =1", new { USERID = user_id, POSTID = data.Post_ID });
                    if (checkName > 0)
                        return 2;
                    else
                    {
                        var _data = await dbConnection.QueryFirstOrDefaultAsync<UserInfo>("SELECT Username FROM [dbo].[User] WHERE User_ID = @USERID and IsEnable = 1", new { USERID = user_id });
                        int status = await dbConnection.ExecuteAsync("INSERT INTO [dbo].[Vote] (User_ID,Username,Post_ID,VoteType,IsEnable) VALUES " +
                        "(@USERID,@USERNAME,@POSTID,2,1)", new
                        { USERID = user_id, USERNAME = _data.Username, POSTID = data.Post_ID });
                        if (status == 1)
                            return 1;
                        else
                            return 0;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> PostFalse(UserVote data, int user_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int checkName = await dbConnection.QueryFirstOrDefaultAsync<int>("SELECT User_ID FROM [dbo].[Vote] WHERE User_ID =  @USERID and Post_ID = @POSTID and IsEnable =1", new { USERID = user_id, POSTID = data.Post_ID });
                    if (checkName > 0)
                        return 2;
                    else
                    {
                        var _data = await dbConnection.QueryFirstOrDefaultAsync<UserInfo>("SELECT Username FROM [dbo].[User] WHERE User_ID = @USERID and IsEnable = 1", new { USERID = user_id });
                        int status = await dbConnection.ExecuteAsync("INSERT INTO [dbo].[Vote] (User_ID,Username,Post_ID,VoteType,IsEnable) VALUES " +
                        "(@USERID,@USERNAME,@POSTID,0,1)", new
                        { USERID = user_id, USERNAME = _data.Username, POSTID = data.Post_ID });
                        if (status == 1)
                            return 1;
                        else
                            return 0;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public async Task<bool> PostComment(UserComment data, int user_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var _data = await dbConnection.QueryFirstOrDefaultAsync<UserInfo>("SELECT * FROM [dbo].[User] WHERE User_ID = @USERID", new { USERID = user_id });
                    int status = await dbConnection.ExecuteAsync("INSERT INTO Comment(UserComment_ID,UserComment_Username,Post_ID,Comment,IsEnable,CreateDate) VALUES(@USERIDCOMMENT, @USERNAMECOMMENT, @POSTID, @COMMENT, 1, @DATE)", new { USERIDCOMMENT = user_id, USERNAMECOMMENT = _data.Username, POSTID = data.Post_ID, COMMENT = data.Comments, DATE = DateTime.Now });
                    if (status == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        public async Task<bool> PostLikeComment(UserLikeComment data, int user_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int checklikecomment = await dbConnection.QueryFirstOrDefaultAsync<int>("SELECT Like_ID FROM [dbo].[LikeComment] WHERE UserLike_ID = @USERLIKEID and IsEnable = 0", new { USERLIKEID = user_id });
                    if (checklikecomment == 1)
                    {
                        int status = await dbConnection.ExecuteAsync("UPDATE [dbo].[LikeComment] SET IsEnable = 1 ,LikeAgainDate = @DATE WHERE Comment_ID = @COMMENTID and UserLike_ID = @USERLIKEID", new { COMMENTID = data.Comment_ID, DATE = DateTime.Now, USERLIKEID = user_id });
                        if (status == 1)
                            return true;
                        else
                            return false;
                    }
                    else
                    {
                        var _data = await dbConnection.QueryFirstOrDefaultAsync<UserInfo>("SELECT Username FROM [dbo].[User] WHERE User_ID = @USERID", new { USERID = user_id });
                        int status = await dbConnection.ExecuteAsync("INSERT INTO [dbo].[LikeComment] (UserLike_ID,UserLike_Username,Comment_ID,IsEnable,CreateDate) VALUES(@USERLIKEID,@USERLIKEUSERNAME,@COMMENTID,1,@DATE)",
                            new { USERLIKEID = user_id, USERLIKEUSERNAME = _data.Username, COMMENTID = data.Comment_ID, DATE = DateTime.Now });
                        if (status == 1)
                            return true;
                        else
                            return false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<bool> PostUnLikeComment(UserLikeComment data, int user_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int checklikecomment = await dbConnection.QueryFirstOrDefaultAsync<int>("SELECT Like_ID FROM [dbo].[LikeComment] WHERE UserLike_ID = @USERLIKEID and IsEnable = 1", new { USERLIKEID = user_id });
                    if (checklikecomment == 1)
                    {
                        int status = await dbConnection.ExecuteAsync("UPDATE [dbo].[LikeComment] SET IsEnable = 0 ,UnLikeDate = @DATE WHERE Comment_ID = @COMMENTID and UserLike_ID = @USERLIKEID", new { COMMENTID = data.Comment_ID, DATE = DateTime.Now, USERLIKEID = user_id });
                        if (status == 1)
                            return true;
                        else
                            return false;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> PostLike(UserLike data, int user_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int _count = await dbConnection.QueryFirstOrDefaultAsync<int>("SELECT Like_ID FROM [Like] WHERE UserLike_ID = @USERLIKEID and Post_ID = @POSTID  and IsEnable = 0 ", new { USERLIKEID = user_id, POSTID = data.Post_ID });

                    if (_count > 0)
                    {
                        int status = await dbConnection.ExecuteAsync("UPDATE [dbo].[Like] SET IsEnable = 1, LikeAgainDate = @DATE WHERE UserLike_ID = @USERLIKEID and Post_ID = @POSTID", new
                        { DATE = DateTime.Now, USERLIKEID = user_id, POSTID = data.Post_ID });

                        if (status == 1)
                            return 1;
                        else
                            return 0;
                    }
                    else
                    {
                        var _data = await dbConnection.QueryFirstOrDefaultAsync<UserInfo>("SELECT Username FROM [dbo].[User] WHERE User_ID = @USERID", new { USERID = user_id });
                        int status = await dbConnection.ExecuteAsync("INSERT INTO [dbo].[Like] (UserLike_ID,UserLike_Username,Post_ID,IsEnable,CreateDate) VALUES(@USERLIKEID,@USERLIKEUSERNAME,@POSTID,1,@DATE)",
                            new { USERLIKEID = user_id, USERLIKEUSERNAME = _data.Username, POSTID = data.Post_ID, DATE = DateTime.Now });
                        if (status == 1)
                            return 1;
                        else
                            return 0;
                    }

                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        public async Task<int> PostUnLike(UserLike data, int user_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int _count = await dbConnection.QueryFirstOrDefaultAsync<int>("SELECT Like_ID FROM [Like] WHERE Post_ID = @POSTID and UserLike_ID = @USERLIKEID and IsEnable = 1 ", new { POSTID = data.Post_ID, USERLIKEID = user_id });
                    int _result = 0;
                    if (_count > 0)
                    {
                        int status = await dbConnection.ExecuteAsync("UPDATE [dbo].[Like] SET IsEnable = 0, UnLikeDate = @DATE WHERE UserLike_ID = @USERLIKEID and Post_ID = @POSTID", new
                        { DATE = DateTime.Now, USERLIKEID = user_id, POSTID = data.Post_ID });
                        if (status == 1)
                            _result = 1;
                        else
                            _result = 0;
                    }

                    return _result;
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        public async Task<int> PostUsernameCheck(UsernameCheck _data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int checkUsername = await dbConnection.QueryFirstOrDefaultAsync<int>("SELECT User_ID FROM [dbo].[User] WHERE (Username = @USERNAME)", new { USERNAME = _data.Username });
                    if (checkUsername > 0)
                    {
                        return 0;
                    }
                    else
                    {
                        return 1;
                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<int> PostEmailCheck(UserEmail data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int checkName = await dbConnection.QueryFirstOrDefaultAsync<int>("SELECT User_ID FROM [dbo].[User] WHERE (Email=@EMAIL)", new { EMAIL = data.Email });

                    if (checkName > 0)
                        return 0;
                    else
                    {
                        return 1;
                    }

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PostBookmark(UserBookmark data, int user_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int checkPost = await dbConnection.QueryFirstOrDefaultAsync<int>("SELECT Bookmark_ID FROM [dbo].[Bookmark] WHERE BookmarkBy_ID = @USERID and Post_ID = @POSTID and IsEnable = 0", new { USERID = user_id, POSTID = data.Post_ID });
                    int status = 0;
                    if (checkPost > 0)
                    {
                        status = await dbConnection.ExecuteAsync("UPDATE [dbo].[Bookmark] SET IsEnable = 1, BookmarkAgainDate = @DATE  WHERE BookmarkBy_ID = @USERID and Post_ID = @POSTID ", new { DATE = DateTime.Now, USERID = user_id, POSTID = data.Post_ID });
                        if (status > 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        var _data = await dbConnection.QueryFirstOrDefaultAsync<UserInfo>("SELECT Username FROM [dbo].[User] WHERE User_ID = @USERID ", new { USERID = user_id });
                        status = await dbConnection.ExecuteAsync("INSERT INTO [dbo].[Bookmark](BookmarkBy_ID,BookmarkBy_Username,Post_ID,IsEnable) VALUES(@BOOKMARKID,@BOOKMARKNAME,@POSTID,1)",
                            new { BOOKMARKID = user_id, BOOKMARKNAME = _data.Username, POSTID = data.Post_ID });
                        if (status > 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<bool> PostUnBookmark(UserBookmark data, int user_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int checkPost = await dbConnection.QueryFirstOrDefaultAsync<int>("SELECT Bookmark_ID FROM [dbo].[Bookmark] WHERE BookmarkBy_ID = @USERID and Post_ID = @POSTID and IsEnable = 1", new { USERID = user_id, POSTID = data.Post_ID });
                    int status = 0;
                    if (checkPost > 0)
                    {
                        status = await dbConnection.ExecuteAsync("UPDATE [dbo].[Bookmark] SET IsEnable = 0, UnBookmarkDate = @DATE WHERE BookmarkBy_ID = @USERID and Post_ID = @POSTID ", new { DATE = DateTime.Now, USERID = user_id, POSTID = data.Post_ID });
                        if (status > 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> GetSendOTPPasswordToEmail(UserPasswordReset data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int status = await dbConnection.ExecuteAsync("INSERT INTO [dbo].[UserPasswordReset] (User_ID,Email,Token_Key,Verify_Code,IsEnable,Status,ExpireDate) VALUES (@USERID,@EMAIL,@TOKENKEY,@VERIFYCODE,@ISENABLE,@STATUS,@EXPIREDATE)",
                        new { USERID = data.User_ID, EMAIL = data.Email, TOKENKEY = data.Token_Key, VERIFYCODE = data.Verify_Code, ISENABLE = 1, @STATUS = 1, EXPIREDATE = data.ExpireDate });
                    if (status == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> PostCheckOTPPasswordFromWithEmail(CheckOtpResetPwWithEmail data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    string email = "";
                    string phone = "";
                    if (IsValidEmail(data.EmailOrPhone) == true)
                    {
                        email = data.EmailOrPhone;
                        phone = null;
                    }
                    else
                    {
                        email = null;
                        phone = data.EmailOrPhone;
                    }
                    var user = await dbConnection.QueryFirstOrDefaultAsync<UserInfo>("SELECT User_ID, Name, Email, Token, PhoneNumber, Username, Image FROM [dbo].[User] WHERE Email=@EMAIL OR PhoneNumber=@PHONE", new { EMAIL = email, PHONE = phone });
                    if (user == null)
                        return 0;


                    UserPasswordReset obj = await dbConnection.QueryFirstOrDefaultAsync<UserPasswordReset>("SELECT User_ID, Email, Token_Key, Verify_Code, Status, CreateDate, ExpireDate FROM UserPasswordReset WHERE [Token_Key] = @TOKENKEY AND [Verify_Code] = @VERIFYCODE AND [User_ID] = @USERID ",
                                new { TOKENKEY = data.Token_Key, VERIFYCODE = data.Verify_Code, USERID = user.User_ID });
                    if (obj != null)
                    {
                        if (obj.ExpireDate >= DateTime.UtcNow.AddHours(9))
                        {
                            int status = await dbConnection.ExecuteAsync("UPDATE [dbo].[User] SET Password=@PASSWORD WHERE User_ID=@USERID", new { PASSWORD = data.NewPassword, USERID = user.User_ID, });
                            if (status == 1)
                                return 2;
                            else
                                return 0;
                        }
                        else
                            return 1;
                    }
                    else
                        return 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> PostCheckOTPPasswordFromWithOutEmail(CheckOtpResetPwWithOutEmail data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {

                    var dataCheck = await dbConnection.QueryFirstOrDefaultAsync<CheckOtp>("SELECT Tokey_Key AS Reference_Key, Verify_Code AS OTP FROM [dbo].[UserPasswordReset] WHERE Verify_Code=@VERIFYCODE AND DATEDIFF(MINUTE, CreateDate, [ExpireDate]) > 0", new { VERIFYCODE = data.Verify_Code });
                    if (dataCheck == null)
                    {
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> GetSendOTPSignUpToEmail(UserSignUp data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int status = await dbConnection.ExecuteAsync("INSERT INTO [dbo].[UserSignUp] (Email,Token_Key,Verify_Code,IsEnable,Status,ExpireDate) VALUES (@USERID,@EMAIL,@TOKENKEY,@VERIFYCODE,@ISENABLE,@STATUS,@EXPIREDATE)",
                        new { EMAIL = data.Email, TOKENKEY = data.Token_Key, VERIFYCODE = data.Verify_Code, ISENABLE = 1, @STATUS = 1, EXPIREDATE = data.ExpireDate });
                    if (status == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> PostCheckOTPSignUpFromWithEmail(CheckOtpSignUpWithEmail data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    string email = "";
                    string phone = "";
                    if (IsValidEmail(data.EmailOrPhone) == true)
                    {
                        email = data.EmailOrPhone;
                        phone = null;
                    }
                    else
                    {
                        email = null;
                        phone = data.EmailOrPhone;
                    }

                    var dataCheck = await dbConnection.QueryFirstOrDefaultAsync<CheckOtp>("SELECT Email, Tokey_Key AS Reference_Key, Verify_Code AS OTP FROM [dbo].[UserSignUp] WHERE Email = @EMAIL, Verify_Code=@VERIFYCODE AND DATEDIFF(MINUTE, CreateDate, [ExpireDate]) > 0", new { EMAIL = data.EmailOrPhone, VERIFYCODE = data.Verify_Code });

                    if (dataCheck == null)
                    {
                        return 0;
                    }
                    else
                    {
                        var user = await dbConnection.QueryFirstOrDefaultAsync<UserInfo>("SELECT User_ID, Name, Email, Token, PhoneNumber, Username, Image FROM [dbo].[User] WHERE Email=@EMAIL OR PhoneNumber=@PHONE", new { EMAIL = email, PHONE = phone });
                        if (user == null)
                        {
                            int status = await dbConnection.ExecuteAsync("INSERT INTO [dbo].[User](Email, Password) VALUES(@EMAIL, @PASSWORD)", new { EMAIL = data.EmailOrPhone, PASSWORD = data.Password });
                            if (status == 1)
                                return 2;
                            else
                                return 0;
                        }
                        else
                        {
                            return 1;

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> PostCheckOTPSignUpFromWithOutEmail(CheckOtpSignUpWithOutEmail data)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {

                    var dataCheck = await dbConnection.QueryFirstOrDefaultAsync<CheckOtp>("SELECT Tokey_Key AS Reference_Key, Verify_Code AS OTP FROM [dbo].[UserSignUp] WHERE Verify_Code=@VERIFYCODE AND DATEDIFF(MINUTE, CreateDate, [ExpireDate]) > 0", new { VERIFYCODE = data.Verify_Code });
                    if (dataCheck == null)
                    {
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        // METHOD PATCH //

        // METHOD PUT //
        public async Task<bool> PutUserUploadImage(ResultUploadImage _data, int user_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    query.AppendLine("UPDATE [dbo].[User] SET [Image] = @FILENAME, ImagePath = @PATH,UploadDate = @DATE WHERE [USER_ID] = @USERID");
                    int status = await dbConnection.ExecuteAsync(query.ToString(), new { FILENAME = _data.FileName, PATH = _data.ImagePath, USERID = user_id, DATE = DateTime.Now });
                    if (status == 1)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> PutProfile(UserUpdate data, int user_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int checkName = await dbConnection.QueryFirstOrDefaultAsync<int>("SELECT User_ID FROM [dbo].[User] WHERE (Email =@EMAIL OR PhoneNumber =@PHONENUMBER OR Username =@USERNAME) AND User_ID != @USERID", new { EMAIL = data.Email, PHONENUMBER = data.PhoneNumber, USERNAME = data.Username, USERID = user_id });

                    if (checkName > 0)
                        return 0;
                    else
                    {
                        var query = new StringBuilder();
                        var _data = await dbConnection.QueryFirstOrDefaultAsync<UserUpdate>("SELECT * FROM [dbo].[User] WHERE User_ID = @USERID", new { USERID = user_id });
                        data.Name ??= _data.Name;
                        data.Email ??= _data.Email;
                        data.PhoneNumber ??= _data.PhoneNumber;
                        data.Username ??= _data.Username;
                        query.AppendLine("UPDATE [dbo].[User] SET [Name] =@NAME, [Email] =@EMAIL, [PhoneNumber] =@PHONENUMBER, [Username] =@USERNAME, [EditDate] =@UPDATEDATE WHERE [USER_ID] = @USERID");
                        int status = await dbConnection.ExecuteAsync(query.ToString(), new { NAME = data.Name, EMAIL = data.Email, PHONENUMBER = data.PhoneNumber, USERNAME = data.Username, UPDATEDATE = DateTime.Now, USERID = user_id });
                        if (status == 1)
                            return 1;
                        else
                            return 2;
                    }


                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<bool> PutComment(UserCommentPut data, int user_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var _data = await dbConnection.QueryFirstOrDefaultAsync<UserInfo>("SELECT Username FROM [dbo].[User] WHERE User_ID = @USERID", new { USERID = user_id });
                    int status = await dbConnection.ExecuteAsync("UPDATE Comment SET Comment = @COMMENT,EditDate = @DATE WHERE Comment_ID = @COMMENTID and UserComment_ID = @USERID", new { COMMENT = data.Comments, DATE = DateTime.Now, COMMENTID = data.Comment_ID, USERID = user_id });
                    if (status == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        // METHOD DELETE //
        public async Task<bool> DeleteComment(int Comment_id, int user_id)
        {
            try
            {
                using (IDbConnection dBConnecntion = Connection)
                {
                    int status = await dBConnecntion.ExecuteAsync("UPDATE Comment SET IsEnable = 0 WHERE Comment_ID = @COMMENTID and UserComment_ID = @USERID", new { COMMENTID = Comment_id, USERID = user_id });
                    if (status == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        public async Task<bool> DeletePost(int post_id, int user_id)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    int status = await dbConnection.ExecuteAsync("DELETE [dbo].[Bookmark] WHERE Post_ID = @POSTID", new { POSTID = post_id });
                    var _dataComment = await dbConnection.QueryAsync<UserComment>("");
                    foreach (var item in _dataComment)
                    {
                        var _dataCommentDel = await dbConnection.QueryFirstOrDefaultAsync<UserLikeComment>("SELECT * FROM [dbo].[Comment] WHERE Post_ID = @POSTID ", new { POSTID = post_id });
                        status = await dbConnection.ExecuteAsync("DELETE [dbo].[LikeComment] WHERE Comment_ID = @COMMENTID", new { COMMENTID = _dataCommentDel.Comment_ID });
                    }
                    status = await dbConnection.ExecuteAsync("DELETE [dbo].[LikePost] WHERE Post_ID = @POSTID", new { POST = post_id });
                    status = await dbConnection.ExecuteAsync("DELETE [dbo].[Comment] WHERE Post_ID = @POSTID", new { POST = post_id });
                    status = await dbConnection.ExecuteAsync("DELETE [dbo].[Vote] WHERE Post_ID = @POSTID", new { POST = post_id });
                    status = await dbConnection.ExecuteAsync("DELETE [dbo].[Post] WHERE Post_ID = @POSTID and User_ID = @USERID", new { POSTID = post_id, USERID = user_id });
                    status = await dbConnection.ExecuteAsync("DELETE [dbo].[PostImage]  WHERE Post_ID = @POSTID", new { POSTID = post_id });
                    if (status == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        //public async Task<List<Follow>> GetFollower (int user_id)
        //{
        //    try
        //    {
        //        using (IDbConnection dbConnection = Connection)
        //        {
        //            var query = new StringBuilder();
        //            query.AppendLine("SELECT * FROM ");
        //            var result = await dbConnection.QueryAsync<Follow>(query.ToString(), new { });
        //            return result.ToList();
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        throw new Exception(ex.Message);
        //    }
        //}
        public async Task<bool> CreatePost(UserPost _data, int user_id, DateTime createdate)
        {
            try
            {
                var status = false;

                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();

                    int post = await dbConnection.ExecuteAsync("INSERT INTO [dbo].[Post](User_ID,Message,Day,Second,Schedule,IsStatus,IsEnable,CreateDate) VALUES(@USERID,@MESSAGE,@DAY,@SECOND,@SCHEDULE,1,1,@DATE)", new { USERID = user_id, MESSAGE = _data.Message, DAY = _data.Open_Vote.Day, SECOND = _data.Open_Vote.Second, SCHEDULE = _data.Schedule, DATE = createdate });

                    if (post == 1)
                    {
                        int postID = await dbConnection.QueryFirstOrDefaultAsync<int>("SELECT Post_ID FROM [dbo].[Post] WHERE User_ID =@USERID AND Message = '"+ _data.Message+"' AND IsEnable = 1", new { USERID = user_id });

                        var arraySplitHastagMessage = _data.Message.Split('#');
                        foreach (var result in arraySplitHastagMessage)
                        {
                            int checkHastag = await dbConnection.QueryFirstOrDefaultAsync<int>("SELECT COUNT(Name) FROM [dbo].[Tag] WHERE Name =@NAME", new { NAME = result });
                            var tagID = await dbConnection.QueryFirstOrDefaultAsync<int>("SELECT Tag_ID FROM [dbo].[Tag] WHERE Name = @NAME", new { NAME = result });
                            if (checkHastag > 0)
                            {
                                int insertHasTagLog = await dbConnection.ExecuteAsync("INSERT INTO [dbo].[TagLog](User_ID,Post_ID,Tag_ID,IsEnable) VALUES(@USERID,@POSTID,@TAGID,1)", new { USERID = user_id, POSTID = postID, TAGID = tagID });
                            }
                            else
                            {
                                int insertHasTag = await dbConnection.ExecuteAsync("INSERT INTO [dbo].[Tag](Name) VALUES(@NAME)", new { NAME = result });
                                if (insertHasTag == 1)
                                {
                                    int insertHasTagLog = await dbConnection.ExecuteAsync("INSERT INTO [dbo].[TagLog](User_ID,Post_ID,Tag_ID,IsEnable) VALUES(@USERID,@POSTID,@TAGID,1)", new { USERID = user_id, POSTID = postID, TAGID = tagID });
                                }
                            }
                        }
                        return true;
                    }
                    else
                    {
                        return status;
                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PostImagePost(string FileName,string link, int user_id, int i, DateTime createdate)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var query = new StringBuilder();
                    var post_id = await dbConnection.QueryFirstOrDefaultAsync<int>("SELECT Post_ID FROM [dbo].[Post] WHERE User_ID = @USERID and  IsEnable = 1 and CreateDate = @DATE", new { USERID = user_id, DATE = createdate });
                    query.AppendLine("INSERT INTO [dbo].[PostImage](Post_ID,FileName,ImagePath,Sequence,CreateDate) VALUES(@POSTID,@FILENAME,@IMAGEPATH,@ORDER,@DATE)");
                    int ImagePost = await dbConnection.ExecuteAsync(query.ToString(), new { POSTID = post_id, FILENAME = FileName, IMAGEPATH = link, ORDER = i, DATE = createdate });
                    if (ImagePost == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }


        public async Task<List<Explore>> GetExplore(string type)
        {
            
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    var result = type == "all" ?
                    await dbConnection.QueryAsync<Explore>("SELECT b.ImagePath FROM [PicApp].[dbo].[Post] a INNER JOIN  [PicApp].[dbo].[PostImage] b  ON a.Post_ID = b.Post_ID ORDER BY a.User_ID") :
                    await dbConnection.QueryAsync<Explore>("SELECT b.ImagePath FROM [PicApp].[dbo].[Post] a INNER JOIN  [PicApp].[dbo].[PostImage] b  ON a.Post_ID = b.Post_ID WHERE b.PostImage_ID = '5' ORDER BY a.User_ID");
                    
                    return result.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


    }
}
